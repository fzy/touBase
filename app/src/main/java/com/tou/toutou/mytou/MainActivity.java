package com.tou.toutou.mytou;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.tou.toutou.mytou.weight.NoNetView;
import com.tou.toutou.mytou.weight.TimeSoldDialog;
import com.tou.toutou.mytou.wx.WxUtil;
import com.tou.toutou.mytou.wx.WxWithdrawModule;
import com.toutou.tou.base.activity.BaseNetActivity;
import com.toutou.tou.retrofit.*;
import com.toutou.tou.util.StringUtil;
import com.toutou.tou.weight.tabLayout.CustomTabLayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends BaseNetActivity {
    TimeSoldDialog timeSoldDialog;
    @BindView(R.id.customTextView)
    CustomTextView customTextView;
    @BindView(R.id.tableLayout)
    CustomTabLayout tableLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    List<Fragment> fragmentList;
    @Override
    protected int initLayoutID() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean openEventBus() {
        return false;
    }

    @Override
    protected void initValue() {
        tableLayout.setupWithViewPager(viewPager);
        fragmentList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            ColorFragment colorFragment = new ColorFragment();
            int j = i % 3;
            switch (j) {
                case 0:
                    colorFragment.setThemeColor(Color.RED);
                    break;
                case 1:
                    colorFragment.setThemeColor(Color.BLUE);
                    break;
                case 2:
                    colorFragment.setThemeColor(Color.YELLOW);
                    break;
                default:
                    colorFragment.setThemeColor(Color.RED);

            }
            fragmentList.add(colorFragment);
        }
        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return fragmentList.get(i);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return StringUtil.safeString(position * 15032362);
            }
        });
    }

    @OnClick(R.id.customTextView)
    public void customTextView() {
        if (timeSoldDialog == null) {
            timeSoldDialog = new TimeSoldDialog(new TimeSoldDialog.TimeSoldDialogDelegate() {
                @Override
                public void selectOk(int[] times) {
                    String str = times[0] + ":" + times[1] + " -- " + times[2] + ":" + times[3];
                    customTextView.setText(str);
                }
            });
        }
        timeSoldDialog.show(getSupportFragmentManager());
    }

    @OnClick(R.id.tv_clear)
    public void tv_clear() {
        DefaultUtils.userId = 0;
    }

    @OnClick(R.id.tv_noNet)
    public void tv_noNet() {
        showNoNetControl();
    }


    @OnClick(R.id.tv_wx_tx)
    public void tv_wx_tx() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String wxUrl = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers"; //获取退款的api接口
                WxWithdrawModule wxWithdrawModule = new WxWithdrawModule();
                SortedMap<Object, Object> packageParams = new TreeMap<Object, Object>();

                packageParams.put("mch_appid", StringUtil.safeString(wxWithdrawModule.getMch_appid()));         //微信公众号的appid
                packageParams.put("mchid", StringUtil.safeString(wxWithdrawModule.getMchid()));       //商务号
                packageParams.put("nonce_str",StringUtil.safeString(wxWithdrawModule.getNonce_str()));  //随机生成后数字，保证安全性

                packageParams.put("partner_trade_no",StringUtil.safeString(wxWithdrawModule.getPartner_trade_no())); //生成商户订单号
                packageParams.put("openid",StringUtil.safeString(wxWithdrawModule.getOpenid()));            // 支付给用户openid
                packageParams.put("check_name",StringUtil.safeString(wxWithdrawModule.getCheck_name()));    //是否验证真实姓名呢
                packageParams.put("re_user_name", StringUtil.safeString(wxWithdrawModule.getRe_user_name()));//收款用户姓名
                packageParams.put("amount",StringUtil.safeString(wxWithdrawModule.getAmount()));            //企业付款金额，单位为分
                packageParams.put("desc",StringUtil.safeString(wxWithdrawModule.getDesc()));    			   //企业付款操作说明信息。必填。
                packageParams.put("spbill_create_ip",StringUtil.safeString(wxWithdrawModule.getSpbill_create_ip())); //调用接口的机器Ip地址
                // TODO: 2019/1/3 生成自己的签名
                String sign = "afdgersa";
                packageParams.put("sign", StringUtil.safeString(sign));

                String wx_xml = WxUtil.getRequestXml(packageParams);



                RequestBody requestBody = RequestBody.create(MultipartBody.ALTERNATIVE, wx_xml);
                OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
                okHttpClient.newCall(new Request.Builder().post(requestBody).url(wxUrl).build()).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        System.out.println("..............onFailure.wx result : " + e.getMessage());
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String result = response.body().string();
                        System.out.println("..............onResponse.wx result : " + result);
                    }
                });
            }
        }).start();

    }


    @Override
    public void initDataFromServer(boolean refresh) {
        TouApplication.touService.workTypes(1).enqueue(new BaseCallback<AboutUsModule, Object>(new BaseView<AboutUsModule, Object>() {
            @Override
            public void successAll(AboutUsModule aboutUsModule) {

            }

            @Override
            public void successContent(Object o) {

            }

            @Override
            public void loadComplete() {

            }

            @Override
            public void fail(int code, String failString) {

            }
        }) {
            @Override
            public List<Integer> initNoLoginCode() {
                return null;
            }

            @Override
            public int getResponseCode(AboutUsModule body) {
                return 0;
            }

            @Override
            public String getResponseMessage(AboutUsModule body) {
                return null;
            }

            @Override
            public Object getResponseContent(AboutUsModule body) {
                return null;
            }

            @Override
            public void hasLogOutAndDeal() {

            }
        });
    }

    NoNetView noNetView;

    @Override
    public void showNoNetControl() {
        ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView();
        noNetView = new NoNetView(this, this);
        viewGroup.addView(noNetView);
    }

    @Override
    public void missNoNetControl() {
        if (noNetView != null && noNetView.getParent() != null) {
            ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView();
            viewGroup.removeView(noNetView);
        }
    }
}
