package com.tou.toutou.mytou;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.toutou.tou.delegate.LoginSuccessInterface;
import com.toutou.tou.weight.singleClick.LoginConfig;
import com.toutou.tou.weight.singleClick.SingleClickTextView;

/**
 * Created by tou on 2018/12/26.
 */

public class CustomTextView extends SingleClickTextView {
    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean isEmptyUserAndToLogin(Context con, LoginSuccessInterface loginSuccessInterface) {
        return LoginUtils.isEmptyUserAndToLogin(loginSuccessInterface);
    }
}
