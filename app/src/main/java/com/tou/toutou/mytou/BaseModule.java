package com.tou.toutou.mytou;

/**
 * Created by tou on 2018/12/26.
 */

public abstract class BaseModule extends com.toutou.tou.retrofit.BaseModule {
    int code;
    String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
