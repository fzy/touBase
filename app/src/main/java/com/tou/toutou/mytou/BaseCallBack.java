package com.tou.toutou.mytou;

import com.toutou.tou.delegate.BaseServerInterface;
import com.toutou.tou.delegate.LoginSuccessInterface;
import com.toutou.tou.retrofit.BaseCallback;
import com.toutou.tou.retrofit.BaseView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tou on 2018/12/26.
 */

public class BaseCallBack<T extends BaseModule, C> extends BaseCallback<T, C> {
    BaseServerInterface baseServerInterface;
    public BaseCallBack(BaseView<T, C> baseView, BaseServerInterface baseServerInterface) {
        super(baseView);
        this.baseServerInterface = baseServerInterface;
    }

    public BaseCallBack(BaseView<T, C> baseView, String saveKey, BaseServerInterface baseServerInterface) {
        super(baseView, saveKey);
        this.baseServerInterface = baseServerInterface;
    }

    @Override
    public List<Integer> initNoLoginCode() {

        return new ArrayList<Integer>(){{add(0);}};
    }

    @Override
    public int getResponseCode(T body) {
        return body.getCode();
    }

    @Override
    public String getResponseMessage(T body) {
        return body.getMessage();
    }

    @SuppressWarnings("unchecked")
    @Override
    public C getResponseContent(T body) {
        return (C) body.getContent();
    }

    @Override
    public void hasLogOutAndDeal() {
        LoginUtils.isEmptyUserAndToLogin(new LoginSuccessInterface() {
            @Override
            public void loginSuccessBack() {
                baseServerInterface.initDataFromServer(true);
            }
        });
    }
}
