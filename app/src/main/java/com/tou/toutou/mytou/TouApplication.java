package com.tou.toutou.mytou;

import android.app.Application;

import com.toutou.tou.application.ApplicationUtil;
import com.toutou.tou.delegate.RetrofitInitInterface;

import retrofit2.Retrofit;

/**
 * Created by tou on 2018/12/26.
 */

public class TouApplication extends Application {
    public static TouService touService;

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationUtil.init(this, 750, 1334, R.layout.view_loadding,
                R.drawable.bg_rectangle_none, R.drawable.bg_circle_none, R.drawable.bg_rectangle_none,R.drawable.bg_circle_none);
        ApplicationUtil.initSmartRefresh();
        ApplicationUtil.initApiService(this, "http://www.bg.com", 30, false, null, null,
                new RetrofitInitInterface() {

                    @Override
                    public void initApiService(Retrofit retrofit) {
                        touService = retrofit.create(TouService.class);
                    }
                });
    }
}
