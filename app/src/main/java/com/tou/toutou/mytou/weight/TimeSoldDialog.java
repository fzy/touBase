package com.tou.toutou.mytou.weight;

import android.annotation.SuppressLint;
import android.view.View;

import com.tou.toutou.mytou.R;
import com.toutou.tou.dialog.BaseDialogFragment;

/**
 * Created by tou on 2018/12/28.
 */

@SuppressLint("ValidFragment")
public class TimeSoldDialog extends BaseDialogFragment {
    View tv_confirm;
    View tv_cancel;
    TimeSlotPickerView timepicker;
    TimeSoldDialogDelegate timeSoldDialogDelegate;
    @Override
    protected int initLayoutID() {
        return R.layout.dialog_time_sole;
    }

    @SuppressLint("ValidFragment")
    public TimeSoldDialog(TimeSoldDialogDelegate timeSoldDialogDelegate){
        this.timeSoldDialogDelegate = timeSoldDialogDelegate;
    }

    @Override
    protected void initViews() {
        tv_confirm = c(R.id.tv_confirm);
        tv_cancel = c(R.id.tv_cancel);
        timepicker = (TimeSlotPickerView) $(R.id.timepicker);
    }

    @Override
    protected void onClickView(int viewId) {
        dismiss();
        switch (viewId){
            case R.id.tv_confirm:
                int times[] = timepicker.getPickerTimes();
                if(timeSoldDialogDelegate != null){
                    timeSoldDialogDelegate.selectOk(times);
                }
                break;
                default:
        }
    }

    public interface TimeSoldDialogDelegate{
        void selectOk(int times[]);
    }
}
