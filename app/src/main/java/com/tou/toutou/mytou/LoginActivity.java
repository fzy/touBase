package com.tou.toutou.mytou;

import com.toutou.tou.base.activity.BaseNextActivity;
import com.toutou.tou.util.ToastUtil;
import com.toutou.tou.weight.singleClick.LoginConfig;

import butterknife.OnClick;

public class LoginActivity extends BaseNextActivity {
    @Override
    protected int initLayoutID() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean openEventBus() {
        return false;
    }

    @OnClick(R.id.bt_login)
    public void login(){
        DefaultUtils.userId = 1;
        ToastUtil.showToast("登陸成功");
        finish();
        if(LoginConfig.loginSuccessInterface != null){
            LoginConfig.loginSuccessInterface.loginSuccessBack();
        }
    }
}
