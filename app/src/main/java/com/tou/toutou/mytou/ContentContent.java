package com.tou.toutou.mytou;

/**
 * Created by 投投 on 2018/3/28.
 */

public class ContentContent {

    /**
     * qq : 591711737
     * phone : 15827331412
     * mobile : 1111111111
     * wechat : 111111111
     * email : 591711737@qq.com
     */

    private String qq;
    private String phone;
    private String mobile;
    private String wechat;
    private String email;

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}