package com.tou.toutou.mytou;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by tou on 2018/12/26.
 */

public interface TouService {
    /**
     * 关于我们
     *
     * 
     */
    @GET("api/baseDate/getOurs")
    Call<AboutUsModule> getUsInfo();


    @POST("/index.php/newCategorys")//工程种类
    Call<AboutUsModule> workTypes(@Query("method") int method);
}
