package com.tou.toutou.mytou.weight;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.tou.toutou.mytou.R;
import com.toutou.tou.weight.picker_view.JumpNumericWheelAdapter;
import com.toutou.tou.weight.picker_view.adapter.NumericWheelAdapter;
import com.toutou.tou.weight.picker_view.lib.WheelViewDate;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tou on 2018/12/28.
 * 上班时间段选择器
 */

public class TimeSlotPickerView extends RelativeLayout {
    @BindView(R.id.wv_hour_start)
    WheelViewDate wvHourStart;
    @BindView(R.id.wv_min_start)
    WheelViewDate wvMinStart;
    @BindView(R.id.wv_hour_end)
    WheelViewDate wvHourEnd;
    @BindView(R.id.wv_min_end)
    WheelViewDate wvMinEnd;

    public TimeSlotPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TimeSlotPickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_time_slot_picker, this);
        ButterKnife.bind(view, this);
        wvHourStart.setDividerType(WheelViewDate.DividerType.WRAP);
        wvHourEnd.setDividerType(WheelViewDate.DividerType.WRAP);
        wvMinStart.setDividerType(WheelViewDate.DividerType.WRAP);
        wvMinEnd.setDividerType(WheelViewDate.DividerType.WRAP);
        wvHourStart.setAdapter(new NumericWheelAdapter(0, 23));
        wvHourEnd.setAdapter(new NumericWheelAdapter(0, 23));
        wvMinStart.setAdapter(new JumpNumericWheelAdapter(0, 10, 20, 30, 40, 50));
        wvMinEnd.setAdapter(new JumpNumericWheelAdapter(0, 10, 20, 30, 40, 50));

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TimeSlotPickerView);
        int startHour = typedArray.getInteger(R.styleable.TimeSlotPickerView_startHour, 0);
        int startMin = typedArray.getInteger(R.styleable.TimeSlotPickerView_startMin, 0);
        int endHour = typedArray.getInteger(R.styleable.TimeSlotPickerView_endHour, 0);
        int endMin = typedArray.getInteger(R.styleable.TimeSlotPickerView_endMin, 0);
        typedArray.recycle();

        setSlotPicker(startHour, startMin, endHour, endMin);
    }

    public void setSlotPicker(int startHour, int startMin, int endHour, int endMin){
        wvHourStart.setCurrentItem(startHour);
        wvMinStart.setCurrentItem((int)(startMin/10));
        wvHourEnd.setCurrentItem(endHour);
        wvMinEnd.setCurrentItem((int)(endMin/10));
    }

    /**
     * 返回时间选择结果int[4]
     * */
    public int[] getPickerTimes(){
        return new int[]{wvHourStart.getCurrentItem(),
                wvMinStart.getCurrentItem() * 10,
                wvHourEnd.getCurrentItem(),
                wvMinEnd.getCurrentItem() * 10};
    }
}
