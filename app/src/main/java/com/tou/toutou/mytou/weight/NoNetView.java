package com.tou.toutou.mytou.weight;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.tou.toutou.mytou.R;
import com.toutou.tou.base.activity.BaseNetActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ViewConstructor")
public class NoNetView extends RelativeLayout {
    BaseNetActivity baseNetActivity;
    public NoNetView(Context context, BaseNetActivity baseNetActivity) {
        super(context);
        this.baseNetActivity = baseNetActivity;
        View view = LayoutInflater.from(context).inflate(R.layout.view_no_net, this);
        ButterKnife.bind(view, this);
    }

    @OnClick(R.id.tv_retry)
    public void onViewClicked() {
        if(baseNetActivity != null){
            baseNetActivity.reloadAndControlNet();
        }
    }
}
