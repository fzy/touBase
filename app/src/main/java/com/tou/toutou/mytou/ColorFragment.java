package com.tou.toutou.mytou;

import android.support.annotation.ColorInt;
import android.widget.LinearLayout;

import com.toutou.tou.base.fragment.BaseLazyFragment;

import butterknife.BindView;

/**
 * Created by tou on 2018/12/29.
 *
 */

public class ColorFragment extends BaseLazyFragment {

    @BindView(R.id.layout)
    LinearLayout layout;

    @ColorInt int themeColor;

    @Override
    protected int initLayoutID() {
        return R.layout.fragment_color;
    }

    @Override
    protected boolean openEventBus() {
        return false;
    }

    public void setThemeColor(int themeColor) {
        this.themeColor = themeColor;
    }


    @Override
    protected void loadData() {
        layout.setBackgroundColor(themeColor);
    }
}
