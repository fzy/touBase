package com.tou.toutou.mytou;

import android.content.Context;
import android.view.ViewGroup;

import com.toutou.tou.adapter.BaseEmptyAdapterParent;
import com.toutou.tou.viewholder.BaseViewHolder;

/**
 * Created by tou on 2018/12/26.
 */

public class BaseEmptyAdapter extends BaseEmptyAdapterParent {
    public BaseEmptyAdapter(Context context, NormalAdapterDelegate normalAdapterDelegate) {
        super(context, normalAdapterDelegate);
    }

    @Override
    protected BaseViewHolder initEmptyViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected BaseViewHolder initFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }
}
