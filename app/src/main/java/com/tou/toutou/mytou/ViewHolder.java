package com.tou.toutou.mytou;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.toutou.tou.viewholder.BaseNormalViewHolder;

import butterknife.BindView;

/**
 * Created by tou on 2018/12/26.
 */

public class ViewHolder extends BaseNormalViewHolder<ContentContent> {
    @BindView(R.id.tv_content)
    TextView tv_content;
    public ViewHolder(Context context, View itemView, BaseNormalDelegate baseNormalDelegate) {
        super(context, itemView, baseNormalDelegate);
    }

    @Override
    public void bindData(ContentContent data) {
        tv_content.setText(data.getMobile());
    }
}
