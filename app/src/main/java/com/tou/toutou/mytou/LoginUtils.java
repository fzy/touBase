package com.tou.toutou.mytou;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.toutou.tou.delegate.LoginSuccessInterface;
import com.toutou.tou.util.ActivityManagerUtil;
import com.toutou.tou.util.ToastUtil;
import com.toutou.tou.weight.singleClick.LoginConfig;

/**
 * Created by tou on 2018/12/26.
 */

public class LoginUtils {
    public static boolean isEmptyUserAndToLogin(LoginSuccessInterface loginSuccessInterface){
        if(DefaultUtils.userId == 0){
            LoginConfig.loginSuccessInterface = loginSuccessInterface;
            Activity activity = ActivityManagerUtil.getActivityManager().currentActivity();
            if(activity != null){
                activity.startActivity(new Intent(activity, LoginActivity.class));
            }
            return true;
        }
        return false;
    }
}
