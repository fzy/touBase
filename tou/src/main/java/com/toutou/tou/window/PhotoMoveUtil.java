package com.toutou.tou.window;

import java.io.File;

/**
 * Created by 投投 on 2018/7/26.
 * android 本地图片处理
 */
public class PhotoMoveUtil {
    /**
     *
     * @param formUrl C:\0.3.3.3-Android切图
     * @param toUrl D:\androidProject\android\app\src\main\res\mipmap-mdpi
     *              D:\androidProject\android\app\src\main\res\mipmap-xdpi
     *              D:\androidProject\android\app\src\main\res\mipmap-xxhdpi
     * @param specialCode "" ， @2x， @3x
     */
    public static void moveNativeToProject(String formUrl, String toUrl, String specialCode){
        File file_ui = new File(formUrl);
        File file_android = new File(toUrl);
        for(File file : file_ui.listFiles()){
            if(!file.isDirectory()){
                String las_name = file.getName();
                if(specialCode != null && specialCode.length() > 0){
                    if(las_name.contains(specialCode)){
                        String now_name = las_name.replace(specialCode,"").trim().toLowerCase();
                        moveFile(file, file_android, file_ui, now_name, las_name, specialCode);
                    }
                } else {
                    if(las_name.length() > 0) {
                        if (!las_name.contains("@")) {
                            String now_name = las_name.trim().toLowerCase();
                            moveFile(file, file_android, file_ui, now_name, las_name, specialCode);
                        }
                    }
                }
            }
        }
    }

    private static void moveFile(File file, File file_android, File file_ui, String now_name, String las_name, String specialCode){
        boolean success = file.renameTo(new File(file_android, now_name));
        System.out.println("rename file -- " + las_name + " -- " + now_name + " ? " + success);
        if(!success){
            if(specialCode == null || specialCode.length() == 0){
                specialCode = "@1x";
            }
            File file_child = new File(file_ui, specialCode);
            if(file_child.exists()){
                System.out.println("    move file -- " + las_name + " -- " + now_name + " ? " + file.renameTo(new File(file_child, now_name)));
            } else{
                System.out.println("    create file -- " + file_child.getName() + " ? " + file_child.mkdir());
                System.out.println("    move file -- " + las_name + " -- " + now_name + " ? " + file.renameTo(new File(file_child, now_name)));
            }
        }
    }
}
