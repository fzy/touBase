package com.toutou.tou.retrofit;

import android.text.TextUtils;

import com.toutou.tou.util.SharedPreferenceUtil;
import com.toutou.tou.util.StringUtil;

/**
 * Created by 投投 on 2018/6/11.
 */

public class NativeDataUtil<C>{
    /**
     * 返回本地缓存数据
     * */
    public void reBackNativePreference(BaseView baseView, String getKey){
        if(!TextUtils.isEmpty(getKey)){
            String str_data = SharedPreferenceUtil.getShareJson(getKey);
            if(!TextUtils.isEmpty(str_data)){
                baseView.loadComplete();
                baseView.successContent((C) StringUtil.string2Object(str_data));
            }
        }
    }
}
