package com.toutou.tou.retrofit;


/**
 * Created by 投投 on 2018/3/28.
 */

public interface BaseView<M,C> {
    void successAll(M m);//网络请求正常情况下的所有数据
    void successContent(C c);//返回的content内数据
    void loadComplete();//网络请求结束
    void fail(int code, String failString);//code 不为0时的数据返回，包括网络请求失败-1


}
