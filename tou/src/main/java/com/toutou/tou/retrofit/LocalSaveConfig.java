package com.toutou.tou.retrofit;

import android.text.TextUtils;

import com.toutou.tou.util.SharedPreferenceUtil;
import com.toutou.tou.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class LocalSaveConfig {
    public static void addSaveKey(String saveKey) {
        String localSaveKey = SharedPreferenceUtil.getShareString("save_key");
        if (!localSaveKey.contains(saveKey)) {
            List<String> localSaveList = getSaveKeyList();
            if (!localSaveList.contains(saveKey)) {
                localSaveList.add(saveKey);
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < localSaveList.size(); i++) {
                    stringBuilder.append(localSaveList.get(i));
                    if (i < localSaveList.size() - 1) {
                        stringBuilder.append(",");
                    }
                }
                SharedPreferenceUtil.setShareString("save_key", stringBuilder.toString());
            }
        }
    }

    public static List<String> getSaveKeyList(){
        String localSaveKey = SharedPreferenceUtil.getShareString("save_key");
        List<String> localSaveList;
        if (TextUtils.isEmpty(localSaveKey)) {
            localSaveList = new ArrayList<>();
        } else {
            localSaveList = StringUtil.getListFromStr(localSaveKey, ",");
            if (localSaveList == null) {
                localSaveList = new ArrayList<>();
            }
        }
        return localSaveList;
    }

    public static void clearSave(){
        List<String> localSaveList = getSaveKeyList();
        for(String saveKey : localSaveList){
            SharedPreferenceUtil.setShareString(saveKey, "");
        }
    }

    public static long getLocalShareMemory() {
        List<String> localSaveList = getSaveKeyList();
        List<Integer> integerList = new ArrayList<>();
        for(String str : localSaveList){
            integerList.add(getStringMemory(str));
        }
        long memoryInteger = 0;
        for(Integer integer : integerList){
            memoryInteger += integer;
        }
        return memoryInteger;
    }

    private static int getStringMemory(String key){
        if(TextUtils.isEmpty(key)){
            return 0;
        }
        String value = SharedPreferenceUtil.getShareString(key);
        return value.getBytes().length;
    }
}
