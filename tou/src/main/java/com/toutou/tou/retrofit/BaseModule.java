package com.toutou.tou.retrofit;

public abstract class BaseModule {
    public abstract Object getContent();
}
