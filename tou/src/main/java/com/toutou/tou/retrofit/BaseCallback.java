package com.toutou.tou.retrofit;


import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.toutou.tou.R;
import com.toutou.tou.delegate.ApiRequestInterface;
import com.toutou.tou.util.ActivityManagerUtil;
import com.toutou.tou.util.SharedPreferenceUtil;
import com.toutou.tou.util.StringUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseCallback<T extends BaseModule, C> implements Callback<T>, ApiRequestInterface<T, C> {

    private BaseView<T, C> baseView;
    private boolean wantUseCurrentData;
    private String saveKey;

    /**
     * 不做緩存
     */
    public BaseCallback(BaseView<T, C> baseView) {
        this.baseView = baseView;
    }

    /**
     * 本地緩存網絡數據
     */
    public BaseCallback(BaseView<T, C> baseView, String saveKey) {
        this.baseView = baseView;
        wantUseCurrentData = true;
        this.saveKey = saveKey;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        boolean responseFlag = true;
        String message = "";
        int code = -1;
        baseCallComplete();
        if (response.body() == null) {
            return;
        }
        baseCallbackAll(response.body());
        if (!response.isSuccessful()) {
            //服务器返回错误编号，直接从errorBody中获取
            responseFlag = false;
            message = response.message();
        } else if (getResponseCode(response.body()) != 0) {
            responseFlag = false;
            //标识错误,从body中取
            code = getResponseCode(response.body());
            message = getResponseMessage(response.body());
        }
        if (responseFlag) {
            //结果成功
            if (wantUseCurrentData && !TextUtils.isEmpty(saveKey)) {
                LocalSaveConfig.addSaveKey(saveKey);
                SharedPreferenceUtil.setShareString(saveKey, StringUtil.object2String(getResponseContent(response.body())));
            }
            baseCallBackContent(getResponseContent(response.body()));
        } else {
            //失败
            if ("null".equals(message)) {
                message = null;
            }
            baseCallbackFail(code, message);
            if (initNoLoginCode() != null && initNoLoginCode().contains(code)) {
                hasLogOutAndDeal();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        t.printStackTrace();
        baseCallComplete();
        String error = "";
        Context context = ActivityManagerUtil.getActivityManager().currentActivity();
        if (context != null) {
            error = context.getString(R.string.net_error);
        }
        baseCallbackFail(-1, error);
    }

    public void baseCallbackAll(T t) {
        if (baseView == null) {
            return;
        }
        baseView.successAll(t);
    }

    public void baseCallBackContent(C c) {
        if (baseView == null) {
            return;
        }
        baseView.successContent(c);
    }

    public void baseCallbackFail(int code, String failReasonStr) {
        if (baseView == null) {
            return;
        }
        baseView.fail(code, failReasonStr);
    }

    public void baseCallComplete() {
        if (baseView == null) {
            return;
        }
        baseView.loadComplete();
    }

}
