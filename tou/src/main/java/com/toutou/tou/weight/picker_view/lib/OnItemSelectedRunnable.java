package com.toutou.tou.weight.picker_view.lib;

final class OnItemSelectedRunnable implements Runnable {
    final WheelViewDate loopView;

    OnItemSelectedRunnable(WheelViewDate loopview) {
        loopView = loopview;
    }

    @Override
    public final void run() {
        loopView.onItemSelectedListener.onItemSelected(loopView.getCurrentItem());
    }
}
