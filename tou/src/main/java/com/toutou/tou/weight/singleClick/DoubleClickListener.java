package com.toutou.tou.weight.singleClick;

public interface DoubleClickListener {
    long doubleClickTimes = 600;
    void doubleClick();
}
