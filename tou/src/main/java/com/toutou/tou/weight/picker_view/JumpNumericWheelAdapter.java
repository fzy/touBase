package com.toutou.tou.weight.picker_view;

import com.toutou.tou.weight.picker_view.adapter.WheelAdapter;

/**
 * Created by tou on 2019/1/25.
 */

public class JumpNumericWheelAdapter implements WheelAdapter {

    Integer[] integers;

    public JumpNumericWheelAdapter(Integer... numbers){
        this.integers = numbers;
    }


    @Override
    public int getItemsCount() {
        return integers == null ? 0 : integers.length;
    }

    @Override
    public Object getItem(int index) {
        return integers[index];
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        for(int i = 0; i < integers.length ; i ++){
            if(integers[i].equals(o)){
                index = i;
                break;
            }
        }
        return index;
    }
}
