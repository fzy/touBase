package com.toutou.tou.weight.singleClick;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import com.toutou.tou.R;
import com.toutou.tou.delegate.JudgeAndLoginInterface;
import com.toutou.tou.delegate.LoginSuccessInterface;


/**
 * 防双击布局, 不建议在该布局上加手势布局
 * */
public abstract class SingleLinearLayout extends LinearLayout implements LoginSuccessInterface, JudgeAndLoginInterface {
    boolean judgeLogin;//点击事件是否要判断登陆并跳转逻辑

    public SingleLinearLayout(Context context) {
        super(context);
    }

    public SingleLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);init(attrs);
    }

    public SingleLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);init(attrs);
    }

    private void init(@Nullable AttributeSet attrs){
        @SuppressLint("CustomViewStyleable")
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.singleClickView);
        judgeLogin = typedArray.getBoolean(R.styleable.singleClickView_judgeLogin, false);
        typedArray.recycle();
    }

    public void setJudgeLogin(boolean judgeLogin) {
        this.judgeLogin = judgeLogin;
    }

    DoubleClickListener onDoubleClickListener;
    public void setOnDoubleClickListener(DoubleClickListener onDoubleClickListener) {
        this.onDoubleClickListener = onDoubleClickListener;
    }

    private long lastClickTime;//先前一次点击时间

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP){
            long nowClickTime = System.currentTimeMillis();
            if (nowClickTime - lastClickTime < DoubleClickListener.doubleClickTimes) {
                lastClickTime = nowClickTime;
                if (onDoubleClickListener != null) {
                    onDoubleClickListener.doubleClick();
                }
                return true;
            } else {
                lastClickTime = nowClickTime;
                if (judgeLogin) {
                    return isEmptyUserAndToLogin(getContext(), this) || super.dispatchTouchEvent(event);
                } else return super.dispatchTouchEvent(event);
            }
        } else return super.dispatchTouchEvent(event);
    }

    @Override
    public void loginSuccessBack() {
        callOnClick();
    }
}
