package com.toutou.tou.weight.singleClick;

import android.annotation.SuppressLint;

import com.toutou.tou.delegate.LoginSuccessInterface;

public class LoginConfig {
    /**
     * 点击并跳往登陆页面的view
     * */
    @SuppressLint("StaticFieldLeak")
    public static LoginSuccessInterface loginSuccessInterface;
}
