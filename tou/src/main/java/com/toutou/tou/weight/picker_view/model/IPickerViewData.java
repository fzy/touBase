package com.toutou.tou.weight.picker_view.model;

/**
 * Created by Sai on 2016/7/13.
 */
public interface IPickerViewData {
    String getPickerViewText();
}
