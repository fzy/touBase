package com.toutou.tou.weight.picker_view.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
