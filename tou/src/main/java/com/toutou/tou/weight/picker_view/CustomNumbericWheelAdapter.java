package com.toutou.tou.weight.picker_view;


import com.toutou.tou.weight.picker_view.adapter.NumericWheelAdapter;

/**
 * Created by tou on 2019/1/25.
 */

public class CustomNumbericWheelAdapter extends NumericWheelAdapter {
    String endStr;
    public CustomNumbericWheelAdapter(int minValue, int maxValue, String endStr){
        super(minValue, maxValue);
        if(endStr == null){
            endStr = "";
        }
        this.endStr = endStr;
    }

    @Override
    public Object getItem(int index) {
        return super.getItem(index) + endStr;
    }
}
