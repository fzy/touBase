package com.toutou.tou.weight.picker_view.listener;

/**
 * Created by Sai on 15/8/9.
 */
public interface OnDismissListener {
    public void onDismiss(Object o);
}
