package com.toutou.tou.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.toutou.tou.R;
import com.toutou.tou.viewholder.BaseViewHolder;

/**
 * Created by toutou on 2018/7/19.
 * 自动处理空数据适配器
 */

public abstract class BaseEmptyAdapterParent extends BaseRecyclerAdapter {
    protected Context c;
    protected boolean noMore;//尾部
    protected BaseStaggeredDelegate baseStaggeredDelegate;

    public BaseEmptyAdapterParent(Context context, NormalAdapterDelegate normalAdapterDelegate) {
        super(normalAdapterDelegate);
        this.c = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (list == null || list.size() == 0) {
            return -100;
        } else if (noMore && isFooterItem(position)) {
            return -200;
        } else {
            return super.getItemViewType(position);
        }
    }

    @Override
    public int getItemCount() {
        if (list == null || list.size() == 0) {
            return 1;
        } else if (noMore) {
            return list.size() + 1;
        } else
            return super.getItemCount();
    }

    protected abstract BaseViewHolder initEmptyViewHolder(ViewGroup parent, int viewType);

    protected abstract BaseViewHolder initFooterViewHolder(ViewGroup parent, int viewType);

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == -100) {
            if(null == initEmptyViewHolder(parent, viewType)){
                return new BaseViewHolder(LayoutInflater.from(c).inflate(R.layout.layout_load_empty, parent, false));
            } else {
                return initEmptyViewHolder(parent, viewType);
            }
        } else if (viewType == -200) {
            if(null == initFooterViewHolder(parent, viewType)){
                return new BaseViewHolder(LayoutInflater.from(c).inflate(R.layout.view_nomore_foot, parent, false));
            } else {
                return initFooterViewHolder(parent, viewType);
            }
        } else
            return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (baseStaggeredDelegate != null) {
            baseStaggeredDelegate.handleLayoutIfStaggeredGridLayout(holder, position);
        }
        if (getItemViewType(position) != -100 && getItemViewType(position) != -200)
            super.onBindViewHolder(holder, position);
    }

    public void setNoMore(boolean noMore) {
        this.noMore = noMore;
    }

    public boolean isNoMore() {
        return noMore;
    }

    public interface BaseStaggeredDelegate {
        void handleLayoutIfStaggeredGridLayout(RecyclerView.ViewHolder holder, int position);
    }
}

