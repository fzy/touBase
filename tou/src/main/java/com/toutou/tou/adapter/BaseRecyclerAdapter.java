package com.toutou.tou.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toutou on 2018/7/19.
 * 多样式适配器
 */

public class BaseRecyclerAdapter extends RecyclerView.Adapter {
    private NormalAdapterDelegate normalAdapterDelegate;
    protected List<Object> list;

    public BaseRecyclerAdapter(NormalAdapterDelegate normalAdapterDelegate) {
        this.normalAdapterDelegate = normalAdapterDelegate;
        if (list == null) {
            list = new ArrayList<Object>();
        }
    }

    public void clear() {
        if (list == null) {
            list = new ArrayList<Object>();
        }
        list.clear();
        notifyDataSetChanged();
    }

    public void add(int index, Object t) {
        if (list == null) {
            list = new ArrayList<Object>();
        }
        list.add(index, t);
        notifyDataSetChanged();
    }

    public void add(Object t) {
        if (list == null) {
            list = new ArrayList<Object>();
        }
        list.add(t);
        notifyDataSetChanged();
    }

    public int getPositionOfFirstObject(Object obj) {
        if (null == list) {
            return -1;
        }
        for (int i = 0; i < list.size(); i++) {
            if (getItem(i).getClass() == obj.getClass()) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 移除position开始到后面的所有数据
     */
    public void clearFromPosition(int position) {
        if (list != null) {
            while (list.size() > position) {
                list.remove(position);
                notifyDataSetChanged();
            }
        }
    }

    public void addAll(List ts) {
        if (list == null) {
            list = new ArrayList<Object>();
        }
        list.addAll(ts);
        notifyDataSetChanged();
    }

    public void setList(List ts) {
        clear();
        addAll(ts);
    }

    public List getData() {
        return list;
    }


    public void remove(Object o) {
        if (null != list && list.contains(o)) {
            list.remove(o);
            notifyDataSetChanged();
        }
    }

    public void remove(int position) {
        if (null != list && list.size() > position) {
            list.remove(position);
            notifyDataSetChanged();
        }
    }

    public void insteadObject(Object oldObj, Object newObj) {
        int position = 0;
        for (int i = 0; i < list.size(); i++) {
            if (getItem(i) == oldObj) {
                position = i;
            }
        }
        remove(oldObj);
        list.add(position, newObj);
        notifyDataSetChanged();
    }

    public Object getItem(int position) {
        if (position < list.size()) {
            return list.get(position);
        } else {
            if(list.size() > 0){
                return list.get(0);
            } else {
                return null;
            }
        }
    }

    public boolean isFooterItem(int position){
        return position >= list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (normalAdapterDelegate != null) {
            return normalAdapterDelegate.getItemViewType(position);
        } else {
            return -1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return normalAdapterDelegate.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        normalAdapterDelegate.onBindViewHolder(holder, position);

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public interface NormalAdapterDelegate {
        int getItemViewType(int position);

        RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType);

        void onBindViewHolder(RecyclerView.ViewHolder holder, int position);
    }

}
