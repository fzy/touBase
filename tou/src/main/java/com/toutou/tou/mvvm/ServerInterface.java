package com.toutou.tou.mvvm;

/**
 * Created by tou on 2019/5/15.
 */

public interface ServerInterface {
    //访问服务器，获取数据 -> 关于本地缓存，在网络框架中集成
    void loadDataFromServer();

    //显示请求进度框
    void upLoadProgress(float speedOfProgress);

    //消失请求进度框
    void missLoadProgress();



}
