package com.toutou.tou.delegate;

import android.content.Context;

/**
 * Created by tou on 2018/12/25.
 * 判断是否已经登陆接口
 */

public interface JudgeAndLoginInterface {
    /**
     * @return bool true 已经登陆， false 未登录，并跳转到登陆
     * */
    boolean isEmptyUserAndToLogin(Context con, LoginSuccessInterface loginSuccessInterface);
}
