package com.toutou.tou.delegate;

import retrofit2.Retrofit;

/**
 * Created by tou on 2018/12/26.
 */

public interface RetrofitInitInterface {
    void initApiService(Retrofit retrofit);
}
