package com.toutou.tou.delegate;

import com.toutou.tou.retrofit.BaseModule;

import java.util.List;

/**
 * Created by tou on 2018/12/26.
 */

public interface ApiRequestInterface<T extends BaseModule,C> {
    List<Integer> initNoLoginCode();
    int getResponseCode(T body);
    String getResponseMessage(T body);
    C getResponseContent(T body);
    void hasLogOutAndDeal();
}
