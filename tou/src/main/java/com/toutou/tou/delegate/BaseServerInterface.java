package com.toutou.tou.delegate;

/**
 * Created by tou on 2018/12/26.
 */

public interface BaseServerInterface {
    void initDataFromServer(final boolean refresh);
}
