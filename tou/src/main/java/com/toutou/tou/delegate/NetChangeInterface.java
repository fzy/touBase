package com.toutou.tou.delegate;

import com.toutou.tou.receiver.NetBroadCastReceiver;

/**
 * Created by tou on 2018/12/25.
 * 网络异常处理
 */

public interface NetChangeInterface {
    void registerNetReceiver(NetBroadCastReceiver.NetChangeDelegate netChangeDelegate);

    void reloadAndControlNet();

    /**
     * 注意保护，防止多次回掉导致崩溃
     * */
    void showNoNetControl();

    void missNoNetControl();
}
