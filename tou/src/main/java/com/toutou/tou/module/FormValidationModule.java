package com.toutou.tou.module;

import android.widget.TextView;

public class FormValidationModule {
    boolean isValidationOK;
    TextView errorTextView;
    String errorMessage;

    public FormValidationModule(TextView errorTextView, String errorMessage){
        this.errorTextView = errorTextView;
        this.errorMessage = errorMessage;
    }

    public FormValidationModule(boolean isValidationOK){
        this.isValidationOK = isValidationOK;
    }

    public boolean isValidationOK() {
        return isValidationOK;
    }

    public void setValidationOK(boolean validationOK) {
        isValidationOK = validationOK;
    }

    public TextView getErrorTextView() {
        return errorTextView;
    }

    public void setErrorTextView(TextView errorTextView) {
        this.errorTextView = errorTextView;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
