package com.toutou.tou.util;

import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.toutou.tou.R;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FileUtil {
    public static File getFile(String filePath) {
        return new File(filePath);
    }

    public static File getNewFile(Application application, String filePath) {
        if (filePath == null) {
            return null;
        }
        if (ContextCompat.checkSelfPermission(application, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ToastUtil.showToast(application.getString(R.string.no_save_permission));
            return null;
        }
        filePath = getDiskCacheDir(application) + "/" + filePath;
        File file = new File(filePath);
        if (!file.exists()) {
            if (!file.getParentFile().exists()) {
                if(!file.getParentFile().mkdirs()){
                    ToastUtil.showToast(application.getString(R.string.create_folder_false));
                    return null;
                }
            }
        }

        return file;
    }

    /**
     * 检查文件是否存在
     */
    public static String checkDirPath(String dirPath) {
        if (TextUtils.isEmpty(dirPath)) {
            return "";
        }
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dirPath;
    }

    /**
     * 获取系统文件目录
     */
    public static String getDiskCacheDir(Context context) {
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            cachePath = Environment.getExternalStorageDirectory().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return cachePath;
    }

    public static Uri getUriFromFile(Activity activity, File file) {
        if (file == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {

            return FileProvider.getUriForFile(activity,
                    BuildUtil.getBuildConfigValue("APPLICATION_ID") + ".fileProvider",
                    file);
        } else {
            return Uri.fromFile(file);
        }
    }

    public static File getRealFile(Activity activity, Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String imagePath = null;
        if (scheme == null)
            imagePath = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            imagePath = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = activity.getContentResolver().query(uri,
                    new String[]{MediaStore.Images.ImageColumns.DATA},
                    null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        imagePath = cursor.getString(index);
                        cursor.close();
                    } else {
                        return new File(getPathFromInputStreamUri(activity, uri, System.currentTimeMillis() + ""));
                    }
                }
            }

        }
        if (imagePath == null) {
            return null;
        }
        return new File(imagePath);
    }
    /**
     * 用流拷贝文件一份到自己APP目录下
     *
     * @param context
     * @param uri
     * @param fileName
     * @return
     */
    public static String getPathFromInputStreamUri(Context context, Uri uri, String fileName) {
        InputStream inputStream = null;
        String filePath = null;

        if (uri.getAuthority() != null) {
            try {
                inputStream = context.getContentResolver().openInputStream(uri);
                File file = createTemporalFileFrom(context, inputStream, fileName);
                filePath = file.getPath();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return filePath;
    }

    private static File createTemporalFileFrom(Context context, InputStream inputStream, String fileName) {
        File targetFile = null;

        if (inputStream != null) {
            int read;
            byte[] buffer = new byte[8 * 1024];
            //自己定义拷贝文件路径
            targetFile = new File(getDiskCacheDir(context), fileName);
            if (targetFile.exists()) {
                targetFile.delete();
            }
            ProgressUtil.showCircleProgress(context);
            try {
                OutputStream outputStream = new FileOutputStream(targetFile);

                while ((read = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, read);
                }
                outputStream.flush();

                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                ProgressUtil.missCircleProgress();
            }


        }

        return targetFile;
    }

    /**
     * 限制图片文件大小
     * */
    public static File compress(Activity activity, File file) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        float hh = dm.heightPixels;
        float ww = dm.widthPixels;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        opts.inJustDecodeBounds = false;
        int w = opts.outWidth;
        int h = opts.outHeight;
        int size = 0;
        if (w <= ww && h <= hh) {
            size = 1;
        } else {
            double scale = w >= h ? w / ww : h / hh;
            double log = Math.log(scale) / Math.log(2);
            double logCeil = Math.ceil(log);
            size = (int) Math.pow(2, logCeil);
        }
        opts.inSampleSize = size;
        String filePath = file.getPath();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, opts);
        if(bitmap == null){
            return null;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int quality = 100;
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos);
        System.out.println(baos.toByteArray().length);
        while (baos.toByteArray().length > 5 * 1024 * 1024) {
            baos.reset();
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos);
            quality -= 20;
            System.out.println(baos.toByteArray().length);
        }
        File currentFile = new File(file.getParentFile() + "/" + System.currentTimeMillis() + ".png");
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(currentFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (fileOutputStream == null) {
            return null;
        }
        try {
            baos.writeTo(fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                baos.flush();
                baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return currentFile;
    }

    //保存图片到SD卡
    public static File saveFile(Application application, Bitmap bm, String fileName) throws IOException {

        String imgName = System.currentTimeMillis() + ".jpg"; //随机生成不同的名字
        File jia = getNewFile(application, fileName);              //新创的文件夹的名字
        if (!jia.exists()) {   //判断文件夹是否存在，不存在则创建
            jia.mkdirs();
        }
        File file = new File(jia + "/" + imgName);
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
        bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);

        bos.flush();
        bos.close();

        return file;
    }

    public static File saveBitmapFile(Application application, Bitmap bitmap) {

        File file = getNewFile(application, "curImg.jpg");
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);

            bos.flush();
            bos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;

    }
}
