package com.toutou.tou.util;

import android.app.Activity;
import android.net.Uri;

import java.io.File;

/**
 * Created by Administrator on 2018/8/17 0017.
 */

public class CurrentImageFileUtil {
    public static final String saveFileKey = "curFilePath";
    public static File getCurrentImageFile(Activity activity) {
        boolean bool;
        File file = new File(FileUtil.checkDirPath(FileUtil.getDiskCacheDir(activity)
                + "/image/"), getCurrentImageName());
        bool = file.getParentFile().exists() || file.getParentFile().mkdirs();
        if (bool) {
            saveCurrentFilePath(activity, file.getPath());
            return file;
        } else {
            return null;
        }
    }

    private static void saveCurrentFilePath(Activity activity, String filePath){
        SharedPreferenceUtil.setShareString(saveFileKey, filePath);
    }

    public static File getCurrentFile(Activity activity){
        File file = new File(SharedPreferenceUtil.getShareString(saveFileKey));
        if(!file.exists()){
            return null;
        }
        return file;
    }

    public static Uri getCurrentImageUri(Activity activity){
        File file = getCurrentImageFile(activity);
        return FileUtil.getUriFromFile(activity, file);
    }

    private static String getCurrentImageName(){
        return System.currentTimeMillis() + ".jpg";
    }


}
