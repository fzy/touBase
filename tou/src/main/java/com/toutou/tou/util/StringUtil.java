package com.toutou.tou.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by toutou on 2018/7/19.
 */

public class StringUtil {

    /**
     * 对象转换成string
     */
    public static String object2String(Object o) {
        String value = "";
        try {
            ByteArrayOutputStream navBaos = new ByteArrayOutputStream();
            ObjectOutputStream navOos = new ObjectOutputStream(navBaos);
            navOos.writeObject(o);
            int flags = Base64.DEFAULT;
            value = Base64.encodeToString(navBaos.toByteArray(), flags);
            navBaos.close();
            navOos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * string转换成对象
     */
    public static Object string2Object(String str) {
        Object o = null;
        try {
            int flags = Base64.DEFAULT;
            byte[] bytes = Base64.decode(str, flags);
            ByteArrayInputStream navBais = new ByteArrayInputStream(bytes);
            ObjectInputStream navOis = new ObjectInputStream(navBais);
            o = (Object) navOis.readObject();
            navBais.close();
            navOis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return o;
    }

    /**
     * 获取string值,保证不是null且为字符串类型
     * */
    public static String safeString(Object obj){
        if(null == obj){
            return "";
        }
        return String.valueOf(obj);
    }

    /** 字符串为数字 */
    public static boolean isNumber(String number) {
        if (TextUtils.isEmpty(number))
            return false;
        else {
            Pattern p = Pattern.compile("[0-9]*");
            Matcher m = p.matcher(number);
            return m.matches();
        }
    }

    /**  带小数的数字 */
    public static boolean isDecimal(String number) {
        if (TextUtils.isEmpty(number))
            return false;
        else {
            Pattern p = Pattern.compile("^[-+]?[0-9]+(\\.[0-9]+)?$");
            Matcher m = p.matcher(number);
            if (m.matches())
                return true;
            else
                return false;
        }
    }

    /** 字符串为字母 */
    public static boolean isLetter(String letter) {
        if (TextUtils.isEmpty(letter))
            return false;
        else
            return letter.matches("^[a-zA-Z]*");
    }

    /** 字符串是否含有汉字汉字 */
    public static boolean hasChinese(String str) {
        if (TextUtils.isEmpty(str))
            return false;
        else {
            String regEx = "[\u4e00-\u9fa5]";
            Pattern p = Pattern.compile(regEx);
            Matcher m = p.matcher(str);
            if (m.find())
                return true;
            else
                return false;
        }
    }

    /** 判断数字是奇数还是偶数  */
    public static int isEvenNumbers(String even) {
        if (!TextUtils.isEmpty(even) && isNumber(even)) {
            int i = Integer.parseInt(even);
            if (i % 2 == 0) {
                //偶数
                return 2;
            } else {
                //奇数
                return 1;
            }
        } else {
            //不是奇数也不是偶数
            return 0;
        }
    }

    /** 判断字符串是否字母开头 */
    public static boolean isLetterBegin(String s) {
        if (TextUtils.isEmpty(s))
            return false;
        else {
            char c = s.charAt(0);
            int i = (int) c;
            if ((i >= 65 && i <= 90) || (i >= 97 && i <= 122)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /** 判断字符串是否以指定内容开头 */
    public static boolean startWithMytext(String mytext, String begin) {
        if (TextUtils.isEmpty(mytext) && TextUtils.isEmpty(begin))
            return false;
        else {
            if (mytext.startsWith(begin))
                return true;
            else
                return false;
        }
    }

    /**
     * 判断字符串是否以指定内容结尾
     */
    public static boolean endWithMytext(String mytext, String end) {
        if (TextUtils.isEmpty(mytext) && TextUtils.isEmpty(end))
            return false;
        else {
            if (mytext.endsWith(end))
                return true;
            else
                return false;
        }
    }

    /**
     * 判断字符串中是否含有指定内容
     * @param string  完整字符串
     * @param mytext  关键字
     *  <true 含有   false 不含>
     */
    public static boolean hasMytext(String string, String mytext) {
        if (TextUtils.isEmpty(string) && TextUtils.isEmpty(mytext))
            return false;
        else {
            if (string.contains(mytext))
                return true;
            else
                return false;
        }
    }

    /**
     * 通过关键字将字符串截取成数组
     * @param content
     * @param key
     *    list
     */
    public static List<String> getListFromStr(String content, String key) {
        if (TextUtils.isEmpty(content)) {
            return null;
        }
        List<String> list = new ArrayList<>();
        String[] ary = content.split(key);
        for(String item: ary){
            list.add(item);
        }

        return list;
    }

    /**
     * 获得前一个字符串
     * @param string
     * @param split 通过":"截取
     * 
     */
    public static String cutPreString(String string, String split) {
        String pre = "";
        if (!TextUtils.isEmpty(string)) {
            pre = string.substring(0, string.indexOf(split));
        }
        return pre;
    }


    /**
     * 获得后一个字符串
     * @param string
     * @param split 通过":"截取
     * 
     */
    public static String cutNextString(String string, String split) {
        String pre = "";
        if (!TextUtils.isEmpty(string)) {
            pre = string.substring(string.indexOf(split) + 1, string.length());
        }
        return pre;
    }


    public static Boolean isHttpPic(String input) {
        if (input != null && !"".equals(input) && input.length() > 3) {
            String content = stringCut(input, 1, 4);

            if ("http".equals(content))
                return true;
        }
        return false;
    }

    /**
     * 字符串截取
     * @param input 待截取字符串
     * @param index 截取起始位置 <1为起始位置>
     * @param count 截取位数
     *  截取后字符串
     */
    public static String stringCut(String input, int index, int count) {
        if (input.isEmpty()) {
            return "";
        }
        count = (count > input.length() - index + 1) ? input.length() - index + 1 : count;

        return input.substring(index - 1, index + count - 1);
    }

    /**
     * 替换特殊字符
     * @param content
     * 
     */
    public static String replaceBtoA(String content, String key) {
        if (null == content || "".equals(content))
            return "";
        return content.replaceAll(key, "");
    }

    /**
     * 数字型字符串返回“0”
     * @param content
     * 
     */
    public static String nullToStr0(String content) {
        if (null == content || "".equals(content))
            return "0";
        return content;
    }

    /**
     * 在第二字后面加空格
     * @param text String
     *  返回加空格的字符串
     */
    public static String stringAdd2Space(String text) {
        String result = "";
        result = text.substring(0, 2) + "  " + text.substring(2);
        return result;
    }

    /**
     * 获取本地配置文档
     * */
    public static String getLocalJson(Context context, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    /**
     * 获取textView 字符串
     * */
    public static String getTextStr (TextView textView) {
        if(textView != null && textView.getText() != null){
            return textView.getText().toString().trim();
        }
        return "";
    }

    public static String judgeStringAndConvertToJinDu(String text, int jinDu) {
        if (text == null || text.equals("null")) {
            return "0";
        }
        NumberFormat nf = NumberFormat.getInstance();
        nf.setRoundingMode(RoundingMode.HALF_UP);
        nf.setMaximumFractionDigits(jinDu);
        try {
            return nf.format(
                    new BigDecimal(text.replace(",", "").trim())).
                    replace(",", "");
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return text;
        }
    }

    public static String judgeStringAndConvertToJinDu(BigDecimal bigDecimal, int jinDu) {
        if (bigDecimal == null) {
            return "0";
        }

        return judgeStringAndConvertToJinDu(bigDecimal.toPlainString(), jinDu);
    }

    public static String judgeStringAndConvertToJinDu(Double dou, int jinDu){
        if(dou == null){
            return "0";
        }
        String text = String.valueOf(dou);
        return judgeStringAndConvertToJinDu(text, jinDu);
    }

    public static String judgeStringAndConvertToJinDuDown(String text, int jinDu){
        if (text == null || text.equals("null")){
            return "0";
        }
        NumberFormat nf = NumberFormat.getInstance();
        nf.setRoundingMode(RoundingMode.DOWN);
        nf.setMaximumFractionDigits(jinDu);
        try {
            return nf.format(
                    new BigDecimal(text.replace(",","").trim()))
                    .replace(",", "");
        }catch (NumberFormatException e){
            e.printStackTrace();
            return text;
        }
    }

    public static String judgeStringAndConvertToJinDuDown(BigDecimal bigDecimal, int jinDu){
        if(bigDecimal == null){
            bigDecimal = new BigDecimal("0");
        }
        return judgeStringAndConvertToJinDuDown(bigDecimal.toPlainString(), jinDu);
    }

    public static String judgeStringAndConvertToJinDuDown(CharSequence charSequence, int jinDu){
        String text;
        if(charSequence == null){
            text = null;
        } else {
            text = charSequence.toString();
        }
        return judgeStringAndConvertToJinDuDown(text, jinDu);
    }

    public static String judgeStringAndConvertToLimitJinDu(String text, int jinDu){
        if (text == null || text.equals("null")) {
            return "0";
        }
        try {
            return judgeStringAndConvertToLimitJinDu(new BigDecimal(text.replace(",", "").trim()), jinDu);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return text;
        }
    }

    public static String judgeStringAndConvertToLimitJinDu(BigDecimal bigDecimal, int jinDu) {
        if (bigDecimal == null) {
            return "0";
        }

        return bigDecimal.setScale(jinDu, BigDecimal.ROUND_HALF_UP).toPlainString();
    }

    public static int string2Integer(String str){
        if(!TextUtils.isEmpty(str)){
            try {
                int num = Integer.parseInt(str);
                return num;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static float string2Float(String str){
        if(!TextUtils.isEmpty(str)){
            try {
                float num = Float.parseFloat(str);
                return num;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return 0f;
    }
}
