package com.toutou.tou.util;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

import com.toutou.tou.R;
import com.toutou.tou.base.fragment.BaseScanPhotoFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tou on 2019/4/28.
 */

public class PhotoUtils {
    @SuppressLint("StaticFieldLeak")
    private static BaseScanPhotoFragment baseScanPhotoFragment;

    public static void showPicFragment(@IdRes int layout, int pos, List<String> picList,@NonNull PhotoDelegate photoDelegate) {
        if (baseScanPhotoFragment != null) {
            missPicFragment(photoDelegate);
        }
        android.support.v4.app.FragmentTransaction fragmentTransaction = photoDelegate.initFragmentTransaction();
        baseScanPhotoFragment = new BaseScanPhotoFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("data", (ArrayList<String>) picList);
        bundle.putInt("position", pos);
        baseScanPhotoFragment.setArguments(bundle);
        fragmentTransaction.setCustomAnimations(R.anim.anim_larger_enter, R.anim.anim_smaller_exit, R.anim.anim_larger_enter, R.anim.anim_smaller_exit);
        fragmentTransaction.replace(layout, baseScanPhotoFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public static void missPicFragment(@NonNull PhotoDelegate photoDelegate) {
        if (baseScanPhotoFragment != null) {
            android.support.v4.app.FragmentTransaction transaction = photoDelegate.initFragmentTransaction();
            transaction.setCustomAnimations(R.anim.anim_larger_enter, R.anim.anim_smaller_exit, R.anim.anim_larger_enter, R.anim.anim_smaller_exit);
            transaction.remove(baseScanPhotoFragment).commitAllowingStateLoss();
            baseScanPhotoFragment = null;
        }
    }

    public interface PhotoDelegate{//解决miss之后， transaction需要重新实例
        android.support.v4.app.FragmentTransaction initFragmentTransaction();
    }
}
