package com.toutou.tou.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.hjq.toast.ToastUtils;


/**
 * Toast统一管理类
 */
public class ToastUtil {
    //使用此工具，需要在Application中申明
    public static void init(Application application){
        ToastUtils.init(application);
    }
    /**
     * 显示提示框，防止同一时间多次触发toast，引起toast一直显示的问题
     */
    @SuppressLint("ShowToast")
    public static void showToast(String msg){
        ToastUtils.show(StringUtil.safeString(msg));
    }
}