package com.toutou.tou.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.toutou.tou.R;
import com.toutou.tou.weight.picker_view.TimePickerView;
import com.toutou.tou.weight.picker_view.listener.CustomListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.lang.Integer.parseInt;

public class TimeUtil {
    private static TimePickerView pvCustomTime;

    @SuppressLint("SimpleDateFormat")
    public static void initCustomTimePicker(Context c, TimePickerView.Type type, TimePickerView.OnTimeSelectListener timeSelectListener) {

        /*从0-11
         * setRangDate方法控制起始终止时间(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
         * @description
         *
         * 注意事项：
         * 1.自定义布局中，id为 optionspicker 或者 timepicker 的布局以及其子控件必须要有，否则会报空指针.
         * 具体可参考demo 里面的两个自定义layout布局。
         * 2.因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是
         */
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        Calendar startDate = Calendar.getInstance();
        int nian = parseInt(new SimpleDateFormat("yyyy").format(new Date(getTomorrowBegin())));
        int yue = parseInt(new SimpleDateFormat("MM").format(new Date(getTomorrowBegin())));
        int ri = parseInt(new SimpleDateFormat("dd").format(new Date(getTomorrowBegin())));
        int hour = parseInt(new SimpleDateFormat("HH").format(new Date(getTomorrowBegin())));
        int min = parseInt(new SimpleDateFormat("mm").format(new Date(getTomorrowBegin())));
        startDate.set(nian, yue - 1, ri);
        if (type == TimePickerView.Type.MONTH_DAY_HOUR_MIN) {
            startDate.set(Calendar.HOUR, hour);
            startDate.set(Calendar.MINUTE, min);
        }
        Calendar endDate = Calendar.getInstance();
        endDate.set(2038, 1, 1);
        //时间选择器 ，自定义布局
        if (pvCustomTime != null) {
            pvCustomTime.dismiss();
            pvCustomTime = null;
        }
        pvCustomTime = new TimePickerView.Builder(c, timeSelectListener)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .setLayoutRes(R.layout.pickerview_custom_time, new CustomListener() {

                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_confirm);
                        TextView ivCancel = (TextView) v.findViewById(R.id.tv_cancel);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.returnData();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.dismiss();
                            }
                        });
                    }
                })
                .setType(type)
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .setDividerColor(Color.parseColor("#EFEFEF"))
                .build();
        pvCustomTime.show();
    }
    /**
     * 获取明天的凌晨12点时间戳
     *
     * @return
     */

    private static long getTomorrowBegin() {

        long now = System.currentTimeMillis() / 1000L;

        long daySecond = 60 * 60 * 24;

        long dayTime = now - (now + 8 * 3600) % daySecond + 1 * daySecond;

        return dayTime * 1000;

    }
}
