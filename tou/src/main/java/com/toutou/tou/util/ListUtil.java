package com.toutou.tou.util;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.toutou.tou.adapter.BaseEmptyAdapterParent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;

/**
 * Created by tou on 2018/12/25.
 */

public class ListUtil {
    /**
     * 初始化listView的高度
     */
    public static void fixListViewHeight(ListView listView) {
        // 如果没有设置数据适配器，则ListView没有子项，返回。
        Adapter listAdapter = listView.getAdapter();
        int totalHeight = 0;
        if (listAdapter == null) {
            return;
        }
        for (int index = 0, len = listAdapter.getCount(); index < len; index++) {
            View listViewItem = listAdapter.getView(index, null, listView);
            // 计算子项View 的宽高
            if (listViewItem != null) {
                listViewItem.measure(0, 0);
                // 计算所有子项的高度和
                totalHeight += listViewItem.getMeasuredHeight();
            }

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // listView.getDividerHeight()获取子项间分隔符的高度
        // params.height设置ListView完全显示需要的高度
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static void fixListViewHeight(RecyclerView listView) {
        // 如果没有设置数据适配器，则ListView没有子项，返回。
        RecyclerView.Adapter listAdapter = listView.getAdapter();
        int totalHeight = 0;
        if (listAdapter == null) {
            return;
        }
        for (int index = 0, len = listAdapter.getItemCount(); index < len; index++) {
            View listViewItem = listView.getChildAt(index);
            // 计算子项View 的宽高
            if (listViewItem != null) {
                listViewItem.measure(0, 0);
                // 计算所有子项的高度和
                totalHeight += listViewItem.getMeasuredHeight();
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // listView.getDividerHeight()获取子项间分隔符的高度
        // params.height设置ListView完全显示需要的高度
        params.height = totalHeight;
        listView.setLayoutParams(params);
    }

    /**
     * list 排重
     * */
    public static List removeDoubleFromList(List list){
        List newList = new ArrayList();
        if(null != list){
            for (Object obj : list) {
                if (!newList.contains(obj)) {
                    newList.add(obj);
                }
            }
        }
        return newList;
    }


    public static void enableRefresh(@NonNull SmartRefreshLayout smartRefreshLayout, BaseEmptyAdapterParent baseEmptyAdapterParent, int pageIndex, int limit, int total) {
        enableRefresh(smartRefreshLayout, baseEmptyAdapterParent, !isFinish(pageIndex, limit, total));
    }

    /**
     * 控制底部无更多布局展示
     */
    public static void enableRefresh(@NonNull SmartRefreshLayout smartRefreshLayout, BaseEmptyAdapterParent baseEmptyAdapterParent, boolean hasMore) {
        if (!hasMore) {
            smartRefreshLayout.setEnableLoadMore(false);
            baseEmptyAdapterParent.setNoMore(true);
        } else {
            smartRefreshLayout.setEnableLoadMore(true);
            baseEmptyAdapterParent.setNoMore(false);
        }
    }

    /**
     * 一个接口数据是否请求完毕
     */
    public static boolean isFinish(int pageIndex, int limit, int total) {
        return pageIndex * limit >= total;
    }

    /**
     * 瀑布流缺省,底部占屏设置
     */
    public static void staggeredRecyclerResetNormal(BaseEmptyAdapterParent baseEmptyAdapterParent, RecyclerView.ViewHolder holder, int position) {
        if (baseEmptyAdapterParent.getItemViewType(position) == -100 || baseEmptyAdapterParent.getItemViewType(position) == -200) {
            StaggeredGridLayoutManager.LayoutParams lp = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            lp.setFullSpan(true);
            holder.itemView.setLayoutParams(lp);
        }
    }

    /**
     * @param list 获取list移除第一个数据后的对象, 但不影响list的值
     */
    public static List filterListFirst(List list) {
        List resultList = new ArrayList();
        for (int i = 1; i < list.size(); i++) {
            resultList.add(list.get(i));
        }
        return resultList;
    }

    /**
     * 普通双列布局间隙控制
     *
     * @param filterPositions  过滤一些位置的控制
     * @param baseEmptyAdapterParent 普通列表页,需要考虑缺省情况的页(不适合有头部的页面).
     */
    public static void gridLayoutManagerRecyclerControlItemSpace(final Context context, final RecyclerView recyclerView, final BaseEmptyAdapterParent baseEmptyAdapterParent, float spaceFloat, Integer... filterPositions) {
        List<Integer> filterPositionList = new ArrayList<>();
        if (filterPositions != null) {
            filterPositionList = Arrays.asList(filterPositions);
        }
        final List<Integer> finalFilterPositionList = filterPositionList;
        final int space = DpUtil.dip2px(context, spaceFloat);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                if (baseEmptyAdapterParent != null && baseEmptyAdapterParent.getData().isEmpty()) {
                    return;
                }
                int position = parent.getChildAdapterPosition(view);
                if (!finalFilterPositionList.contains(position)) {
                    outRect.top = space / 2;
                    if(recyclerView.getLayoutManager() instanceof GridLayoutManager){
                        int distancePosition = position - maxRelyIntegerSmall(finalFilterPositionList, position);
                        if (distancePosition % 2 == 0) {//右边的数据
                            outRect.left = space / 4;
                            outRect.right = space;
                        } else {//左边的数据
                            outRect.left = space;
                            outRect.right = space / 4;
                        }
                    }
                }
            }
        });
    }

    private static int maxRelyIntegerSmall(@NonNull List<Integer> integerList, int curInteger) {
        int resultInteger = -1;
        for (Integer integer : integerList) {
            if (integer != null && integer < curInteger) {
                if (integer > resultInteger) {
                    resultInteger = integer;
                }
            }
        }
        return resultInteger;
    }

    /**
     * recyclerView
     * use @Link{BaseEmptyAdapterParent}
     * 格局布局权重设置(缺省,尾部 横全)
     *
     * @param filterPositions 将这些位置设置为横满
     */
    public static void gridLayoutSpanSizeControl(final GridLayoutManager gridLayoutManager, final BaseEmptyAdapterParent baseEmptyAdapterParent, Integer... filterPositions) {
        if (gridLayoutManager == null || baseEmptyAdapterParent == null) {
            return;
        }
        List<Integer> filterPositionList = new ArrayList<>();
        if (filterPositions != null) {
            filterPositionList = Arrays.asList(filterPositions);
        }
        final List<Integer> finalFilterPositionList = filterPositionList;
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (finalFilterPositionList.contains(position)) {
                    return gridLayoutManager.getSpanCount();
                }
                if (baseEmptyAdapterParent.getItemCount() > 0) {
                    if (baseEmptyAdapterParent.isFooterItem(position)) {
                        return gridLayoutManager.getSpanCount();
                    } else {
                        return 1;
                    }
                } else {
                    return gridLayoutManager.getSpanCount();
                }
            }
        });
    }

    /**
     * 格局布局权重设置(缺省,尾部 横全)
     */
    public static void gridLayoutSpanSizeControl(final GridLayoutManager gridLayoutManager, final BaseEmptyAdapterParent baseEmptyAdapterParent) {
        if (gridLayoutManager == null || baseEmptyAdapterParent == null) {
            return;
        }
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (baseEmptyAdapterParent.getItemCount() > 0) {
                    if (baseEmptyAdapterParent.isFooterItem(position)) {
                        return gridLayoutManager.getSpanCount();
                    } else {
                        return 1;
                    }
                } else {
                    return gridLayoutManager.getSpanCount();
                }
            }
        });
    }

    /**
     * 滚动到顶部
     * @param baseEmptyAdapterParent 如果仅仅是回到顶部功能, 不需要传
     */
    public static void initRecyclerScrollToFirst(@NonNull RecyclerView recyclerView, @NonNull final View iv_goTop, final BaseEmptyAdapterParent baseEmptyAdapterParent, final ListUtil.RecyclerScrollInterface recyclerScrollInterface) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == SCROLL_STATE_IDLE) {
                    int position = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                    if (position > 20) {
                        iv_goTop.animate().alpha(1f);
                    } else {
                        iv_goTop.animate().alpha(0f);
                    }
                    if (null != baseEmptyAdapterParent) {
                        if (baseEmptyAdapterParent.getItemCount() - 20 > 0 && position > baseEmptyAdapterParent.getItemCount() - 20) {
                            if (recyclerScrollInterface != null) {
                                recyclerScrollInterface.canLoadMore();
                            }
                        }
                    }
                } else {
                    iv_goTop.animate().alpha(0f);
                }
            }
        });
    }

    /**
     * 辅助{@link #initRecyclerScrollToFirst(RecyclerView, View, BaseEmptyAdapterParent, RecyclerScrollInterface)}
     */
    public interface RecyclerScrollInterface {
        void canLoadMore();
    }
}
