package com.toutou.tou.util;

import android.app.Activity;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Created by toutou on 2018/7/23.
 */

public class ActivityManagerUtil {
    private static final String TAG = "ActivityManager";
    private static Stack<Activity> activityStack;
    private static Set<Activity> filterActivitys;
    private static ActivityManagerUtil instance;
    private Activity currActivity;

    private ActivityManagerUtil() {
    }

    public static ActivityManagerUtil getActivityManager() {
        if (instance == null) {
            instance = new ActivityManagerUtil();
        }
        return instance;
    }

    public void addFilterActivity(Activity activity) {
        if (filterActivitys == null) {
            filterActivitys = new HashSet<>();
        }
        filterActivitys.add(activity);
    }

    public boolean isFilterActivity() {
        return null != filterActivitys && filterActivitys.contains(currentActivity());
    }

    //退出栈顶Activity
    public void popActivity(Activity activity) {
        if (activity == null || activityStack == null) {
            return;
        }
        if (activityStack.contains(activity)) {
            activityStack.remove(activity);
        }
        currActivity = activity;
        //activity.finish;

    }

    public void destroyActivity(Activity activity) {
        if (activity == null) {
            return;
        }
        activity.finish();
        if (activityStack.contains(activity)) {
            activityStack.remove(activity);
        }
        activity = null;
    }

    //获得当前栈顶Activity
    public Activity currentActivity() {
        if (activityStack == null || activityStack.empty()) {
            return null;
        }
        return activityStack.lastElement();
    }

    //将当前Activity推入栈中
    public void pushActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<Activity>();
        }
        activityStack.add(activity);
    }

    //退出栈中除指定的Activity外所有
    public void popAllActivityExceptOne(Class cls) {
        while (true) {
            Activity activity = currentActivity();
            if (activity == null) {
                break;
            } else {
                if (activity.getClass().equals(cls)) {
                    break;
                } else destroyActivity(activity);
            }
        }
    }

    //退出栈中所有Activity

    public void popAllActivity() {
        popAllActivityExceptOne(null);
    }

    public Activity getCurrentActivity() {
        return currActivity;
    }

    public int getActivityStackSize() {
        int size = 0;
        if (activityStack != null) {
            size = activityStack.size();
        }
        return size;
    }

    public boolean isOpenActivity(Class cls) {
        if (activityStack != null) {
            for (int i = 0, size = activityStack.size(); i < size; i++) {
                if (cls == activityStack.get(i).getClass()) {
                    return true;
                }
            }
        }
        return false;
    }
}
