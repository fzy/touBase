package com.toutou.tou.util;

import android.app.Application;
import android.content.Context;

import java.lang.reflect.Field;

/**
 * Created by tou on 2018/12/25.
 */

public class BuildUtil {
    /**
     * 获取主项目的配置值
     *
     * @param fieldName "FLAVOR"
     * @return String
     */
    public static Object getBuildConfigValue(String fieldName) {
        try {
            if(ActivityManagerUtil.getActivityManager().currentActivity() != null){
                Class<?> clazz = Class.forName(ActivityManagerUtil.getActivityManager().currentActivity().getPackageName() + ".BuildConfig");
                Field field = clazz.getField(fieldName);
                return field.get(null);
            }
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getBuildConfigValue(Application application, String fieldName) {
        try {
            Class<?> clazz = Class.forName(application.getPackageName() + ".BuildConfig");
            Field field = clazz.getField(fieldName);
            return field.get(null);
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
