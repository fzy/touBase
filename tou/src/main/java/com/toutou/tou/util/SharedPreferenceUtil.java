package com.toutou.tou.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SP相关工具类
 */
public class SharedPreferenceUtil {

    private static Activity getAppContext(){
        return ActivityManagerUtil.getActivityManager().currentActivity();
    }
    
    /**
     * 存储bool
     *
     * @param key   key
     * @param value 数值
     */
    public static void setShareBoolean(String key, boolean value) {
        if(getAppContext() == null){
            return;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * 取出bool
     *
     * @param key key
     * @return bool
     */
    public static boolean getShareBoolean(String key) {
        if(getAppContext() == null){
            return false;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * 存储String
     *
     * @param key   key
     * @param value 数值
     */
    public static void setShareString(String key, String value) {
        if(getAppContext() == null){
            return;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, StringUtil.safeString(value));
        editor.apply();
    }

    /**
     * 取出String
     *
     * @param key key
     * @return String
     */
    public static String getShareString(String key) {
        if(getAppContext() == null){
            return "";
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        return sharedPreferences.getString(key, "");
    }

    /**
     * 存储String
     *
     * @param key   key
     * @param value 数值
     */
    public static void setShareDouble(String key, double value) {
        if(getAppContext() == null){
            return;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value + "");
        editor.apply();
    }

    /**
     * 取出String
     *
     * @param key key
     * @return String
     */
    public static double getShareDouble(String key) {
        if(getAppContext() == null){
            return 0;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        double value = 0;
        if (StringUtil.isNumber(sharedPreferences.getString(key, ""))) {
            sharedPreferences.getString(key, "");
            value = Double.valueOf(sharedPreferences.getString(key, "0"));
        }
        return value;
    }

    /**
     * 存储String
     *
     * @param key   key
     * @param value 数值
     */
    public static void setShareInt(String key, int value) {
        if(getAppContext() == null){
            return;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value + "");
        editor.apply();
    }

    /**
     * 取出String
     *
     * @param key key
     * @return String
     */
    public static int getShareInt(String key) {
        if(getAppContext() == null){
            return 0;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        int value = 0;
        if (StringUtil.isNumber(sharedPreferences.getString(key, ""))) {
            sharedPreferences.getString(key, "");
            value = Integer.valueOf(sharedPreferences.getString(key, "0"));
        }
        return value;
    }

    /**
     * 存储json
     *
     * @param key   key
     * @param value 数值
     */
    public static void setShareJson(String key, String value) {
        if(getAppContext() == null){
            return;
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * 取出json
     *
     * @param key key
     * @return String
     */
    public static String getShareJson(String key) {
        if(getAppContext() == null){
            return "";
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        return sharedPreferences.getString(key, "");
    }
}