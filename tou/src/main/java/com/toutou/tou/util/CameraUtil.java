package com.toutou.tou.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.qiniu.android.common.FixedZone;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCancellationSignal;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;

import java.io.File;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Administrator on 2018/8/17 0017.
 */

public class CameraUtil {
    private static final int GetPhotoFromCameraRequest = 2;
    private static final int GetPhotoFromAlbumRequest = 1;

    public static void useCameraGetPhoto(Activity activity) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            Uri photoURI = CurrentImageFileUtil.getCurrentImageUri(activity);
            if (photoURI != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(takePictureIntent, GetPhotoFromCameraRequest);
            }
        }
    }

    public static void useAlbumGetPhoto(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        //选择的格式为视频,图库中就只显示视频（如果图片上传的话可以改为image/*，图库就只显示图片）
        intent.setType("image/*");
        // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_GALLERY
        activity.startActivityForResult(intent, GetPhotoFromAlbumRequest);
    }

    public static void getPhotoResult(Activity activity, String token, int requestCode, int resultCode, Intent data, UpCompletionHandler upCompletionHandler) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GetPhotoFromCameraRequest: {
                    File file = CurrentImageFileUtil.getCurrentFile(activity);
                    // 从 相机 获取的图片
                    if (file == null) {
                        return;
                    }
                    Uri pictur = Uri.fromFile(file);
                    upLoadImageToQNServer(activity, token, pictur, upCompletionHandler);
                    break;
                }
                case GetPhotoFromAlbumRequest: {
                    upLoadImageToQNServer(activity, token, data.getData(), upCompletionHandler);
                    break;
                }
                default:
            }
        }
    }

    /**
     * @param token               七牛的token
     * @param upCompletionHandler 上传图片后的回调
     */
    private static void upLoadImageToQNServer(Activity activity, String token, Uri uri, UpCompletionHandler upCompletionHandler) {
        if (null == uri || token == null) return;
        final String scheme = uri.getScheme();
        String imagePath = null;
        if (scheme == null)
            imagePath = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            imagePath = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = activity.getContentResolver().query(uri,
                    new String[]{MediaStore.Images.ImageColumns.DATA},
                    null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        imagePath = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }

        if (imagePath == null) {
            return;
        }
        File uploadFile = new File(imagePath);
        Configuration config = new Configuration.Builder()
                .chunkSize(512 * 1024)        // 分片上传时，每片的大小。 默认256K
                .putThreshhold(1024 * 1024)   // 启用分片上传阀值。默认512K
                .connectTimeout(10)             // 链接超时。默认10秒
                .responseTimeout(60)          // 服务器响应超时。默认60秒
                .zone(FixedZone.zone0)        // 设置区域，指定不同区域的上传域名、备用域名、备用IP。
                .build();
        // 重用uploadManager。一般地，只需要创建一个uploadManager对象
        UploadManager mUploadManager = new UploadManager(config);
        ProgressUtil.showProgress(activity, "上传图片", 0, 100);
        String tokens = token.replace("\\s", "").trim();
        mUploadManager.put(uploadFile, uploadFile.getName(), tokens, upCompletionHandler, new UploadOptions(null, null, false, new UpProgressHandler() {
            @Override
            public void progress(String key, double percent) {
                ProgressUtil.updateProgress((float) (percent * 100));
                if (percent > 0.9) {
                    ProgressUtil.missProgress();
                }
            }
        }, new UpCancellationSignal() {
            @Override
            public boolean isCancelled() {
                return false;
            }
        }));
    }
}
