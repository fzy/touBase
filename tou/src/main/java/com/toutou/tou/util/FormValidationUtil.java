package com.toutou.tou.util;

import android.text.TextUtils;

import com.toutou.tou.module.FormValidationModule;

import java.util.ArrayList;
import java.util.List;

/**
 * 表单验证
 */
public class FormValidationUtil {
    private List<FormValidationModule> validationList;

    public FormValidationUtil(){
        validationList = new ArrayList<>();
    }

    public void addValidationModule(FormValidationModule formValidationModule){
        validationList.add(formValidationModule);
    }

    public void removeValidationModule(FormValidationModule formValidationModule){
        if(validationList.contains(formValidationModule)){
            validationList.remove(formValidationModule);
        }
    }

    public FormValidationModule validationEmpty(){
        for(FormValidationModule formValidationModule : validationList){
            if(TextUtils.isEmpty(formValidationModule.getErrorTextView().getText())){
                formValidationModule.setValidationOK(false);
                return formValidationModule;
            }
        }
        return new FormValidationModule(true);
    }
}
