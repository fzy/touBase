package com.toutou.tou.util;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

import com.toutou.tou.module.VideoInfoBean;

import java.io.File;

/**
 * Created by tou on 2018/12/25.
 */

public class VideoUtil {

    /**
     * 获取视频信息
     * */
    public static VideoInfoBean getVideoInfo(Context c, File file) {
        MediaPlayer mediaPlayer = MediaPlayer.create(c, Uri.fromFile(file));
        VideoInfoBean videoInfoBean = new VideoInfoBean();
        long duration = mediaPlayer == null ? 0 : mediaPlayer.getDuration();
        int height = mediaPlayer == null ? 0 : mediaPlayer.getVideoHeight();
        int width = mediaPlayer == null ? 0 : mediaPlayer.getVideoWidth();
        String thumbNum = file.getPath() + "?vframe/jpg/offset/0";
        videoInfoBean.setThumbNail(thumbNum);
        videoInfoBean.setDuration(duration);
        videoInfoBean.setWidth(width);
        videoInfoBean.setHeight(height);
        return videoInfoBean;
    }
}
