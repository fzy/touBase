package com.toutou.tou.util;

/**
 * Created by tou on 2018/12/25.
 */

public class PhoneUtil {

    public static String jiaMiPhone(String phone) {
        if (phone == null) {
            return "";
        }
        String str_phone = phone.trim();
        if (str_phone.length() == 11) {
            return str_phone.substring(0, 3) + "****" + str_phone.substring(7, str_phone.length());
        }
        return phone;
    }
}
