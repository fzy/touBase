package com.toutou.tou.base.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.toutou.tou.R;

import butterknife.ButterKnife;

/**
 * Created by toutou on 2018/7/19.
 * 页面形式的View层
 */

public abstract class BasePageActivity extends BaseNextActivity {
    protected abstract int initLayoutID();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(initLayoutID());
        ButterKnife.bind(this);
    }


    @Override
    public void startActivity(Intent intent) {
        overridePendingTransition(R.anim.anim_page_up, 0);
        super.startActivity(intent);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.anim_page_down);
    }
}
