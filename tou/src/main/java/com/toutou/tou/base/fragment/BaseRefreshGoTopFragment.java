package com.toutou.tou.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.toutou.tou.R;
import com.toutou.tou.util.ListUtil;


public abstract class BaseRefreshGoTopFragment extends BaseRecyclerFragment{
    View iv_goTop;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iv_goTop = view.findViewById(R.id.iv_goTop);
        iv_goTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
                iv_goTop.animate().alpha(0f);
            }
        });
        ListUtil.initRecyclerScrollToFirst(recyclerView, iv_goTop, baseEmptyAdapterParent, onRecyclerScrollInterface());
    }

    protected ListUtil.RecyclerScrollInterface onRecyclerScrollInterface(){
        return null;
    }
}
