package com.toutou.tou.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Created by toutou on 2018/7/19.
 * 提供懒加载回调
 */

public abstract class BaseLazyFragment extends BaseNextFragment {

    //Fragment的View加载完毕的标记
    private boolean isViewCreated;

    //Fragment对用户可见的标记
    private boolean isUIVisible;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isViewCreated = true;
        lazyLoad();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            isUIVisible = true;
            lazyLoad();
        } else isUIVisible = false;
    }

    protected void lazyLoad(){
        if (isViewCreated && isUIVisible) {
            loadData();
            isViewCreated = false;
            isUIVisible = false;
        }
    }

    protected abstract void loadData();
}
