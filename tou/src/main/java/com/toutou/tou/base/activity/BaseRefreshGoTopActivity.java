package com.toutou.tou.base.activity;

import android.view.View;


import com.toutou.tou.R;
import com.toutou.tou.util.ListUtil;

/**
 * 显示自动回到列表顶部的按钮, 布局页面必须有iv_goTop
 * */
public abstract class BaseRefreshGoTopActivity extends BaseRecyclerActivity{
    View iv_goTop;

    @Override
    protected void doDownFirst() {
        super.doDownFirst();
        iv_goTop = findViewById(R.id.iv_goTop);
        iv_goTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
                iv_goTop.animate().alpha(0f);
            }
        });
        ListUtil.initRecyclerScrollToFirst(recyclerView, iv_goTop, baseEmptyAdapterParent, onRecyclerScrollInterface());
    }

    protected ListUtil.RecyclerScrollInterface onRecyclerScrollInterface(){
        return null;
    }
}
