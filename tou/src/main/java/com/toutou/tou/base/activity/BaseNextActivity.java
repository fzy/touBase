package com.toutou.tou.base.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.toutou.tou.R;
import com.toutou.tou.util.ActivityManagerUtil;
import com.toutou.tou.util.ToolBarUtil;

import butterknife.ButterKnife;
import org.greenrobot.eventbus.EventBus;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;

/**
 * Created by toutou on 2018/7/18.
 * 仿IOS侧滑
 */

public abstract class BaseNextActivity extends SwipeBackActivity{
    protected abstract int initLayoutID();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doUpFirst();
        setContentView(initLayoutID());
        doDownFirst();
    }

    protected void doUpFirst(){
        ActivityManagerUtil.getActivityManager().pushActivity(this);
    }

    protected void doDownFirst(){
        ButterKnife.bind(this);
        ToolBarUtil.changeToWhite(this);
    }

    InputMethodManager imm;

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            //隐藏键盘
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null && imm.isActive()) {
                try {
                    IBinder iBinder = getCurrentFocus().getWindowToken();
                    imm.hideSoftInputFromWindow(iBinder, 0);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right && event.getY() > top && event.getY() < bottom);
        }
        return false;
    }

    protected abstract boolean openEventBus();

    protected void startEventBus() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    protected void stopEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(openEventBus()){
            startEventBus();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopEventBus();
        ActivityManagerUtil.getActivityManager().popActivity(this);

    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
