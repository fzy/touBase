package com.toutou.tou.base.fragment;

import com.toutou.tou.delegate.BaseServerInterface;
import com.toutou.tou.delegate.NetChangeInterface;

/**
 * Created by tou on 2018/12/25.
 */

public abstract class BaseNextServiceFragment extends BaseLazyFragment implements NetChangeInterface, BaseServerInterface{

    @Override
    protected void loadData() {
        initDataFromServer(true);
    }

}
