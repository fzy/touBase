package com.toutou.tou.base.activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.toutou.tou.R;
import com.toutou.tou.adapter.BaseEmptyAdapterParent;
import com.toutou.tou.util.ListUtil;
import com.toutou.tou.util.ProgressUtil;
import com.toutou.tou.util.ToastUtil;

public abstract class BaseRecyclerActivity extends BaseNetActivity {
    protected SmartRefreshLayout smartRefreshLayout;
    protected RecyclerView recyclerView;
    protected BaseEmptyAdapterParent baseEmptyAdapterParent;
    protected int mPageIndex = 1;
    @Override
    protected void doDownFirst() {
        super.doDownFirst();
        smartRefreshLayout = (SmartRefreshLayout) findViewById(R.id.smartRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData(true);
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadData(false);
            }
        });
        initBaseEmptyAdapter();
        recyclerView.setAdapter(baseEmptyAdapterParent);
        initLayoutManager();
    }

    protected void loadData(boolean refresh) {
        if (refresh) {
            mPageIndex = 1;
        } else {
            mPageIndex++;
        }
        initDataFromServer(refresh);
    }

    protected abstract void initBaseEmptyAdapter();

    protected abstract void initLayoutManager();

    /**
     * 数据访问成功后调用
     *
     * @param refresh
     * @param pageIndex
     * @param limit
     * @param total
     */
    public void loadSuccess(boolean refresh, int pageIndex, int limit, int total) {
        loadComplete();
        if (refresh) baseEmptyAdapterParent.clear();
        ListUtil.enableRefresh(smartRefreshLayout, baseEmptyAdapterParent, pageIndex, limit, total);
    }

    /**
     * 数据访问失败后调用
     *
     * @param e
     */
    public void loadError(String e) {
        loadComplete();
        ToastUtil.showToast(e);
    }

    public void loadComplete() {
        ProgressUtil.missCircleProgress();
        smartRefreshLayout.finishRefresh();
        smartRefreshLayout.finishLoadMore();
    }
}
