package com.toutou.tou.base.fragment;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.toutou.tou.R;
import com.toutou.tou.receiver.NetBroadCastReceiver;
import com.toutou.tou.util.NetworkUtil;
import com.toutou.tou.util.ToastUtil;

/**
 * Created by tou on 2018/12/25.
 * 网络处理的碎片
 */

public abstract class BaseNetFragment extends BaseNextServiceFragment implements NetBroadCastReceiver.NetChangeDelegate {
    protected NetBroadCastReceiver netBroadCastReceiver;

    private boolean isDisconnect;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerNetReceiver(this);
    }

    @Override
    public void registerNetReceiver(NetBroadCastReceiver.NetChangeDelegate netChangeDelegate) {
        netBroadCastReceiver = new NetBroadCastReceiver(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        getActivity().registerReceiver(netBroadCastReceiver, filter);
    }

    @Override
    public void reloadAndControlNet() {
        if (!NetworkUtil.isNetWorkConnected(getContext())) {
            ToastUtil.showToast(getString(R.string.no_net_work));
            disconnectNet();
        } else {
            missNoNetControl();
            initDataFromServer(true);
        }
    }

    @Override
    public void connectNet(boolean wirelessNet, boolean mobileNet) {
        //保证界面绘制完毕
        //保证仅调用一次
        if(isDisconnect){
            isDisconnect = false;
            reloadAndControlNet();
        }
    }

    @Override
    public void disconnectNet() {
        isDisconnect = true;
        showNoNetControl();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(NetworkUtil.isNetWorkConnected(getContext()) && isDisconnect){//这里必须在onCreate的最后面, 保证其它逻辑先处理完毕
            isDisconnect = false;
            missNoNetControl();
            reloadAndControlNet();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(null != netBroadCastReceiver){
            getActivity().unregisterReceiver(netBroadCastReceiver);
        }
    }
}
