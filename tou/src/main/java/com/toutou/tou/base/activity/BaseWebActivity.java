package com.toutou.tou.base.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.github.lzyzsd.jsbridge.DefaultHandler;
import com.toutou.tou.R;
import com.toutou.tou.util.StringUtil;

/**
 * Created by tou on 2018/12/28.
 * h5+ 显示界面
 */

public abstract class BaseWebActivity extends BaseNextServiceActivity {
    public static final String MAIN_URL = "MAIN_URL";
    BridgeWebView bridgeWebView;
    boolean showNavigation;//是否显示标题
    @Override
    protected boolean openEventBus() {
        return false;
    }

    @Override
    protected int initLayoutID() {
        return R.layout.activity_base_web;
    }

    /*baidu*/
    protected abstract String ownUrlKey();

    @Override
    protected void doDownFirst() {
        super.doDownFirst();
        bridgeWebView = (BridgeWebView) findViewById(R.id.bridgeWebView);
        initWebView();
        initWebViewListener();
    }

    @Override
    public void initDataFromServer(boolean refresh) {
        String url = getIntent().getStringExtra(MAIN_URL);
        bridgeWebView.loadUrl(url);
        if(!url.contains(StringUtil.safeString(ownUrlKey()))){
            showNavigation = true;
        }
    }


    /**
     * 初始化WebView配置
     */
    @SuppressLint("SetJavaScriptEnabled")
    public void initWebView() {
        bridgeWebView.setDefaultHandler(new DefaultHandler());

        WebSettings settings = bridgeWebView.getSettings();
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setBlockNetworkImage(false);
        settings.setDomStorageEnabled(true);
        String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
        settings.setAppCachePath(appCachePath);
        settings.setAllowFileAccess(true);
        settings.setAppCacheEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        bridgeWebView.setWebChromeClient(initWebChromeClient());
    }

    protected WebChromeClient initWebChromeClient(){
        return new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if(showNavigation){
                    resetTitle(title);
                }
            }

            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String AcceptType, String capture) {
                this.openFileChooser(uploadMsg);
            }

            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String AcceptType) {
                this.openFileChooser(uploadMsg);
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                showTipDialogNoCancel(BaseWebActivity.this, message, result);
                return true;
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                showTipDialog(BaseWebActivity.this, message, result);
                return true;
            }

            @Override
            public boolean onJsPrompt(WebView view, String url, String message,
                                      String defaultValue, JsPromptResult result) {
                showTipDialog(BaseWebActivity.this, message, result);
                return true;
            }
        };
    }

    protected abstract void resetTitle(String title);

    /**
     * 没有取消的提示框
     *
     * @param context
     * @param message
     * @param jsResult
     */
    public static void showTipDialogNoCancel(Context context, String message, final JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);

        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int i) {
                d.dismiss();
                jsResult.confirm();
            }
        });

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                jsResult.cancel();
            }
        });

        builder.show();
    }

    /**
     * 提示框
     *
     * @param context
     * @param message
     * @param jsResult
     */
    public static void showTipDialog(Context context, String message, final JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);

        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int i) {
                d.dismiss();
                jsResult.confirm();
            }
        });

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                jsResult.cancel();
            }
        });

        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
                d.dismiss();
                jsResult.cancel();
            }
        });

        builder.show();

    }

    protected void initWebViewListener(){
        //关闭webView
        bridgeWebView.registerHandler("JSBridge_closeWebView", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                finish();
            }
        });
    };
}
