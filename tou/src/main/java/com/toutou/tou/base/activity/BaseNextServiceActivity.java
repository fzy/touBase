package com.toutou.tou.base.activity;

import com.toutou.tou.delegate.BaseServerInterface;

/**
 * Created by tou on 2018/12/25.
 * 网络服务的基类
 */

public abstract class BaseNextServiceActivity extends BaseNextActivity implements BaseServerInterface {

    @Override
    protected void doDownFirst() {
        super.doDownFirst();
        initValue();
        initDataFromServer(true);
    }

    protected abstract void initValue();
}
