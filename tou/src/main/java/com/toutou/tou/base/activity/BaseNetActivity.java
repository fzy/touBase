package com.toutou.tou.base.activity;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

import com.toutou.tou.R;
import com.toutou.tou.delegate.NetChangeInterface;
import com.toutou.tou.receiver.NetBroadCastReceiver;
import com.toutou.tou.util.NetworkUtil;
import com.toutou.tou.util.ToastUtil;

/**
 * Created by tou on 2018/12/25.
 * 考虑网络异常的基类
 */

public abstract class BaseNetActivity extends BaseNextServiceActivity implements NetChangeInterface, NetBroadCastReceiver.NetChangeDelegate {
    protected NetBroadCastReceiver netBroadCastReceiver;

    private boolean isDisconnect;

    @Override
    protected void doDownFirst() {
        super.doDownFirst();
        registerNetReceiver(this);
    }

    @Override
    public void registerNetReceiver(NetBroadCastReceiver.NetChangeDelegate netChangeDelegate) {
        netBroadCastReceiver = new NetBroadCastReceiver(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(netBroadCastReceiver, filter);
    }

    @Override
    public void reloadAndControlNet() {
        if (!NetworkUtil.isNetWorkConnected(this)) {
            ToastUtil.showToast(getString(R.string.no_net_work));
            disconnectNet();
        } else {
            missNoNetControl();
            initDataFromServer(true);
        }
    }

    @Override
    public void connectNet(boolean wirelessNet, boolean mobileNet) {
        //保证界面绘制完毕
        //保证仅调用一次
        if(isDisconnect){
            isDisconnect = false;
            missNoNetControl();
            reloadAndControlNet();
        }
    }

    @Override
    public void disconnectNet() {
        isDisconnect = true;
        showNoNetControl();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(NetworkUtil.isNetWorkConnected(this) && isDisconnect){//这里必须在onCreate的最后面, 保证其它逻辑先处理完毕
            isDisconnect = false;
            missNoNetControl();
            reloadAndControlNet();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != netBroadCastReceiver) {
            unregisterReceiver(netBroadCastReceiver);
        }
    }
}
