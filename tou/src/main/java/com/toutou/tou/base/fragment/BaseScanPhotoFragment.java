package com.toutou.tou.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.stx.xhb.xbanner.XBanner;
import com.stx.xhb.xbanner.entity.SimpleBannerInfo;
import com.toutou.tou.R;
import com.toutou.tou.util.ImageLoaderUtil;
import com.toutou.tou.weight.photo.PhotoView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tou on 2019/4/28.
 */

public class BaseScanPhotoFragment extends BaseNextFragment {
    XBanner mXBanner;
    View iv_back;
    List<SimpleBannerInfo> simpleBannerInfoList;
    @Override
    protected int initLayoutID() {
        return R.layout.fragment_base_scan_photo;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iv_back = view.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() != null){
                    android.support.v4.app.FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.anim_larger_enter, R.anim.anim_smaller_exit, R.anim.anim_larger_enter, R.anim.anim_smaller_exit);
                    transaction.remove(BaseScanPhotoFragment.this).commitAllowingStateLoss();
                }
            }
        });
        mXBanner = view.findViewById(R.id.xbanner);
        mXBanner.loadImage(new XBanner.XBannerAdapter() {
            @Override
            public void loadBanner(XBanner banner, Object model, View view, int position) {
                final SimpleBannerInfo curBannerInfo = (SimpleBannerInfo) model;
                ImageLoaderUtil.display((ImageView) view.findViewById(R.id.photoView), (ProgressBar) view.findViewById(R.id.progress_bar),(String) curBannerInfo.getXBannerUrl());
                PhotoView photoView = view.findViewById(R.id.photoView);
                photoView.enable();
                photoView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(getActivity() != null){
                            getActivity().onBackPressed();
                        }
                    }
                });
            }
        });
        if(getArguments() != null){
            List<String> imageUrlList = getArguments().getStringArrayList("data");
            simpleBannerInfoList = new ArrayList<>();
            if (imageUrlList != null) {
                for(final String imageUrl : imageUrlList){
                    SimpleBannerInfo simpleBannerInfo = new SimpleBannerInfo() {
                        @Override
                        public Object getXBannerUrl() {
                            return imageUrl;
                        }
                    };
                    simpleBannerInfoList.add(simpleBannerInfo);
                }
            }
            mXBanner.setBannerData(R.layout.layout_scan_photo_item, simpleBannerInfoList);
            int lastPosition = getArguments().getInt("position", 0);
            mXBanner.getViewPager().setCurrentItem(lastPosition);
        }
    }

    @Override
    protected boolean openEventBus() {
        return false;
    }
}
