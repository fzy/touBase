package com.toutou.tou.application;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreator;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreator;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.tencent.bugly.crashreport.CrashReport;
import com.toutou.tou.R;
import com.toutou.tou.delegate.RetrofitInitInterface;
import com.toutou.tou.retrofit.CustomGsonConverterFactory;
import com.toutou.tou.retrofit.CustomTrust;
import com.toutou.tou.util.ActivityManagerUtil;
import com.toutou.tou.util.AppUtil;
import com.toutou.tou.util.FileUtil;
import com.toutou.tou.util.ToastUtil;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.internal.platform.Platform;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * Created by 投投 on 2018/7/26.
 * 初始化时调用init 才可以正常时候tou框架
 */
public class ApplicationUtil {
    public static int loadingLayoutResId;

    public static int placeholderResId;
    public static int placeholderCircleResId;
    public static int errorResId;
    public static int errorCircleResId;
    public static String certsFilePath;

    public static void init(Application application, float ui_width, float ui_height, int loadingResId,
                            int placeholderResId, int placeholderCircleResId, int errorResId, int errorCircleResId) {
        //android 7.0 系统拍照问题
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        initScreen(application, ui_width, ui_height);
        initLoadingLayout(loadingResId);
        initGlideDefaultImage(placeholderResId, placeholderCircleResId, errorResId, errorCircleResId);
    }

    public static void initSmartRefresh() {
        SmartRefreshLayout.setDefaultRefreshHeaderCreator(new DefaultRefreshHeaderCreator() {
            @NonNull
            @Override
            public RefreshHeader createRefreshHeader(@NonNull Context context, @NonNull RefreshLayout layout) {
                layout.setPrimaryColorsId(android.R.color.white, android.R.color.black);//全局设置主题颜色
                return new MaterialHeader(context);//.setTimeFormat(new DynamicTimeFormat("更新于 %s"));//指定为经典Header，默认是 贝塞尔雷达Header
            }
        });
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator(new DefaultRefreshFooterCreator() {
            @NonNull
            @Override
            public RefreshFooter createRefreshFooter(@NonNull Context context, @NonNull RefreshLayout layout) {
                //指定为经典Footer，默认是 BallPulseFooter
                return new ClassicsFooter(context).setDrawableSize(20);
            }
        });
    }

    /**
     * @param baseUrl               服务器地址：http://192.168.1.249:8170/
     * @param timeOut               超时时间
     * @param isHttps               是否支持https
     * @param certsFileInAssetsPath         支持https必填 信任证书在assets中的地址：certs/214694379660210.pem
     * @param mInterceptor          请求头部可添加
     * @param retrofitInitInterface 最后执行Service的初始化
     */
    public static void initApiService(@NonNull Application application, @NonNull String baseUrl,
                                      long timeOut, boolean isHttps, String certsFileInAssetsPath, Interceptor mInterceptor,
                                      @NonNull RetrofitInitInterface retrofitInitInterface) {
        certsFilePath = certsFileInAssetsPath;
        //输出提示信息
        boolean loggable = AppUtil.isDebug(application);

        LoggingInterceptor loggingInterceptor = new LoggingInterceptor.Builder()
                .setLevel(Level.BODY)
                .loggable(loggable)
                .log(Platform.INFO).build();

        File httpCacheDirectory = new File(FileUtil.getDiskCacheDir(application), "goatmailCache");
        Cache cache = new Cache(httpCacheDirectory, 20 * 1024 * 1024);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(loggingInterceptor);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //打印请求参数
                Request request = chain.request();
                String method = request.method();
                String tag = "LoggingI params";
                Log.i(tag, "******************************************"+method+"******************************************");
                RequestBody requestBody = request.body();
                Buffer buffer = new Buffer();
                if (requestBody != null) {
                    requestBody.writeTo(buffer);
                }
                Log.i(tag,buffer.readUtf8());
                buffer.close();
                Log.i(tag, "*********************************************************************************************");
                return chain.proceed(request);
            }
        });
        builder.retryOnConnectionFailure(true);
        builder.connectTimeout(timeOut, TimeUnit.SECONDS);
        builder.readTimeout(timeOut, TimeUnit.SECONDS);
        builder.writeTimeout(timeOut, TimeUnit.SECONDS);
        if (mInterceptor != null) {
            builder.addInterceptor(mInterceptor);
        }
        if (isHttps) {
            CustomTrust ct = new CustomTrust(application);
            ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_1)
                    .tlsVersions(TlsVersion.TLS_1_2)
                    .cipherSuites(
                            CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                            CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                            CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                    .build();
            builder.connectionSpecs(Collections.singletonList(spec))
                    .sslSocketFactory(ct.sslSocketFactory, ct.trustManager)
                    .hostnameVerifier((s, sslSession) -> true);
        }

        OkHttpClient okHttpClient = builder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(CustomGsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        retrofitInitInterface.initApiService(retrofit);
    }

    private void initSafeAppUiAndTXBugly(Application application, String buglyKey) {
        boolean finalLoggable = AppUtil.isDebug(application);
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(application);
        AppUtil.AppInfo appInfo = AppUtil.getAppInfo(application);
        if(appInfo != null){
            strategy.setUploadProcess(AppUtil.isAppAlive(application, appInfo.getPackageName()));
        } else {
            strategy.setUploadProcess(false);
        }
        if (!finalLoggable) {
            CrashReport.initCrashReport(application, buglyKey, false, strategy);
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() { //主线程异常拦截
                while (true) {
                    try {
                        Looper.loop();//主线程的异常会从这里抛出
                    } catch (Throwable e) {
                        e.printStackTrace();
                        Log.e("mainError", e.getMessage());
                        if(!finalLoggable){
                            CrashReport.postCatchedException(e);
                        }
                        ActivityManagerUtil.getActivityManager().currentActivity().finish();
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.showToast(application.getString(R.string.error_ui));
                            }
                        });
                    }
                }
            }
        });

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                Log.e("threadError", thread.getName(), throwable);
                if(!finalLoggable){
                    CrashReport.postCatchedException(throwable, thread);
                }
            }
        });

    }
    //region Helper

    /**
     * @param width  750
     * @param height 1334
     */
    private static void initScreen(Application application, float width, float height) {
        new ScreenAdaptation(application, width, height).register();
    }

    private static void initLoadingLayout(int resID) {
        loadingLayoutResId = resID;
    }

    private static void initGlideDefaultImage(int placeholderRes, int placeholderCircleRes,
                                              int errorRes, int errorCircleRes) {
        placeholderResId = placeholderRes;
        placeholderCircleResId = placeholderCircleRes;
        errorResId = errorRes;
        errorCircleResId = errorCircleRes;
    }

    //endregion


}
