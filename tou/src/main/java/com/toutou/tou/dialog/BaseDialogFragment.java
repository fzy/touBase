package com.toutou.tou.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.toutou.tou.R;


/**
 * Created by tou on 2018/12/28.
 */

public abstract class BaseDialogFragment extends DialogFragment implements View.OnClickListener{
    protected Dialog dialog;
    View parentView;
    protected abstract int initLayoutID();

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if(getActivity() == null){
            return super.onCreateDialog(savedInstanceState);
        }
        dialog = new Dialog(getActivity(), R.style.DialogFragment);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // 设置Content前设定
        parentView = LayoutInflater.from(getActivity()).inflate(initLayoutID(), null);
        dialog.setContentView(parentView);
        dialog.setCanceledOnTouchOutside(true); // 外部点击取消
        initViews();
        initLayoutParams();
        return dialog;
    }

    protected abstract void initViews();

    protected View $(@IdRes int viewId){
        if(parentView == null){
            return new View(getActivity());
        }
        return parentView.findViewById(viewId);
    }

    protected View c(@IdRes int viewId){
        if(parentView == null){
            return new View(getActivity());
        }
        View view = parentView.findViewById(viewId);
        if(view != null){
            view.setOnClickListener(this);
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        onClickView(v.getId());
    }

    protected abstract void onClickView(@IdRes int viewId);

    // 设置窗体尺寸
    protected void initLayoutParams() {
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            limitHeight(lp);
            lp.gravity = Gravity.BOTTOM;
            window.setAttributes(lp);
        }
    }

    protected void limitHeight(WindowManager.LayoutParams lp){
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    public void show(FragmentManager manager){
        //打印调用栈
        StackTraceElement[] stack = new Throwable().getStackTrace();
        for (StackTraceElement element : stack){
            System.out.println(" |----" + element.toString());
        }
        //这里直接调用show方法会报java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
        FragmentTransaction ft = manager.beginTransaction();
        if(isAdded()){
            ft.hide(this);
        }
        ft.add(this, "");
        ft.commitAllowingStateLoss();
        manager.executePendingTransactions();
    }
}
