package com.toutou.tou.viewholder;

import android.content.Context;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseNormalViewHolder<T> extends BaseViewHolder {
    protected Context context;
    protected BaseNormalDelegate baseNormalDelegate;

    public BaseNormalViewHolder(Context context, View itemView, BaseNormalDelegate baseNormalDelegate) {
        super(itemView);
        this.context = context;
        this.baseNormalDelegate = baseNormalDelegate;
        ButterKnife.bind(this, itemView);
    }

    public abstract void bindData(T data);

    public interface BaseNormalDelegate{}
}
