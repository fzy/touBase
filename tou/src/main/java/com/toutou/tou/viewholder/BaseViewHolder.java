package com.toutou.tou.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by toutou on 2018/7/19.
 */

public class BaseViewHolder extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
    }
}
