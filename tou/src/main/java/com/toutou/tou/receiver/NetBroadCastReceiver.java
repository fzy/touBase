package com.toutou.tou.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

public class NetBroadCastReceiver extends BroadcastReceiver {
    NetChangeDelegate netChangeDelegate;

    boolean wifi;
    boolean internet;

    public NetBroadCastReceiver(NetChangeDelegate netChangeDelegate) {
        this.netChangeDelegate = netChangeDelegate;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //判断是否连接网络
        ConnectivityManager con = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
        if (con != null) {
            wifi = con.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
            internet = con.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
            if (wifi || internet) {
                if (netChangeDelegate != null) {
                    netChangeDelegate.connectNet(wifi, internet);
                }
            } else {
                if (netChangeDelegate != null) {
                    netChangeDelegate.disconnectNet();
                }
            }
        } else {
            if (netChangeDelegate != null) {
                netChangeDelegate.disconnectNet();
            }
        }
    }

    public interface NetChangeDelegate {
        //后续预留手机/无线 网络切换接口
        void connectNet(boolean wirelessNet, boolean mobileNet);

        void disconnectNet();
    }
}

